package com.ezekielbaniaga.tcptester;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerSocketApp {

	public static void main(String[] args) {
		try {
			int sockCount=0;
			ServerSocket sock = new ServerSocket(12345);
			while(true) {
				sock.accept();
				System.out.println(++sockCount);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
