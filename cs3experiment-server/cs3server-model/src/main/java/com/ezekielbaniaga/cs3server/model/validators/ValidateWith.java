package com.ezekielbaniaga.cs3server.model.validators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateWith {

	ValidateLogic[] value();
	
}
