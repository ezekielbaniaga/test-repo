package com.ezekielbaniaga.cs3server.model.internal;

public final class PageInfo {
	public int page, pageSize;
	
	public PageInfo(int page, int pageSize) {
		this.page = page;
		this.pageSize = pageSize;
	}

	/**
	 * If page properties less than 0, 
	 * We set to return all results.
	 */
	public void normalize() {
		if (page < 0 || pageSize < 0) {
			page = 0;
			pageSize = Integer.MAX_VALUE;
		}
	}
}
