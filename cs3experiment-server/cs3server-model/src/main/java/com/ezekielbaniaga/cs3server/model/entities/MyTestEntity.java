package com.ezekielbaniaga.cs3server.model.entities;

import com.ezekielbaniaga.cs3server.model.validators.AlphaSpaceOnlyValidator;
import com.ezekielbaniaga.cs3server.model.validators.MyTestEntityExistsValidator;
import com.ezekielbaniaga.cs3server.model.validators.StringNotBlankValidator;
import com.ezekielbaniaga.cs3server.model.validators.ValidateLogic;
import com.ezekielbaniaga.cs3server.model.validators.ValidateWith;

@ValidateWith({
	@ValidateLogic(validator=MyTestEntityExistsValidator.class, message="Test entity already exists!")
})
public class MyTestEntity {

	@ValidateWith({
		@ValidateLogic(validator=StringNotBlankValidator.class, message="test1 field must not be blank")
	})
	public String test1;

	@ValidateWith({
		@ValidateLogic(validator=AlphaSpaceOnlyValidator.class, message="test2 field must only contain alpha characters and/or space.")
	})
	public String test2;
	
	public int myAge;
}
