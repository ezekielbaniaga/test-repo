package com.ezekielbaniaga.cs3server.model.validators;

public interface Validator<T> {
	boolean isNotValid(T param);
}
