package com.ezekielbaniaga.cs3server.model.validators;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateLogic {

	Class<? extends Validator<?>> validator();
	
	String message() default "no message implemented";
	
	boolean skipValidationIfNull() default false;
}
