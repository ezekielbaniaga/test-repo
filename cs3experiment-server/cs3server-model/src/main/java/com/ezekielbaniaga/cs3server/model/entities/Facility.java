package com.ezekielbaniaga.cs3server.model.entities;

import com.ezekielbaniaga.cs3server.model.validators.AlphaSpaceOnlyValidator;
import com.ezekielbaniaga.cs3server.model.validators.FacilityExistsValidator;
import com.ezekielbaniaga.cs3server.model.validators.StringNotBlankValidator;
import com.ezekielbaniaga.cs3server.model.validators.ValidateLogic;
import com.ezekielbaniaga.cs3server.model.validators.ValidateWith;

@ValidateWith({
	@ValidateLogic(validator=FacilityExistsValidator.class, message="Facility already exists!")
})
public class Facility {

	public Long facilityId;
	
	@ValidateWith({ 
		@ValidateLogic(validator=AlphaSpaceOnlyValidator.class, message="Facility name must contain only alpha characters and/or space."),
		@ValidateLogic(validator=StringNotBlankValidator.class, message="Facility name must not be blank.")
	})
	public String facilityName;

	public String facilityLogo;

	public Float blendedRate;

	public MedicalSystem medicalSystem;

	public Address address;

	public Boolean withChargeCodes;

	public Boolean imageExportEnabled;

	public Boolean canFinalizeOutsideOfSystem;

	public Boolean active;

}
