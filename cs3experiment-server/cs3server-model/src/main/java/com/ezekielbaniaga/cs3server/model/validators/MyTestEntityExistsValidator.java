package com.ezekielbaniaga.cs3server.model.validators;

import com.ezekielbaniaga.cs3server.model.entities.MyTestEntity;

public interface MyTestEntityExistsValidator extends Validator<MyTestEntity> {

}
