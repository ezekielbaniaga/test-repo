package com.ezekielbaniaga.cs3server.model.validators;

import com.ezekielbaniaga.cs3server.model.entities.Facility;

public interface FacilityExistsValidator extends Validator<Facility> {
}
