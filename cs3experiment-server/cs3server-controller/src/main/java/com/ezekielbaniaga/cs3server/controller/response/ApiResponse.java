package com.ezekielbaniaga.cs3server.controller.response;

import org.apache.commons.lang3.StringUtils;

public class ApiResponse {
	public boolean success;
	public String message;
	public Object data;
	
	public static ApiResponse createSuccessResponse(Object data) {
		ApiResponse response = new ApiResponse();
		response.success = true;
		response.data = data;

		return response;
	}
	
	public static ApiResponse createFailedResponse(String[] message) {
		ApiResponse response = new ApiResponse();
		response.success = false;
		response.data = null;
		response.message = StringUtils.join(message, "\n");
		
		return response;
	}
}
