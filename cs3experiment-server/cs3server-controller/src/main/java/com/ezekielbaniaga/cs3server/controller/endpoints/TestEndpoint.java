package com.ezekielbaniaga.cs3server.controller.endpoints;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ezekielbaniaga.cs3server.controller.response.ApiResponse;

@Controller
@RequestMapping("/test")
public class TestEndpoint {

	@ResponseBody
	@RequestMapping(method=RequestMethod.GET)
	public ApiResponse test1() {
		return ApiResponse.createSuccessResponse("Test Data 2");
	}
}
