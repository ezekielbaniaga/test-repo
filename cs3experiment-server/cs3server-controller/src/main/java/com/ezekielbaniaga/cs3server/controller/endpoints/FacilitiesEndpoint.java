package com.ezekielbaniaga.cs3server.controller.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ezekielbaniaga.cs3server.controller.response.ApiResponse;
import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.logic.services.FacilitiesService;
import com.ezekielbaniaga.cs3server.model.custom.FacilityName;
import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.model.internal.PageInfo;

@Controller
@RequestMapping("/facilities")
public class FacilitiesEndpoint {
	
	@Autowired
	FacilitiesService facilitiesService;

	@ResponseBody
	@RequestMapping(method=RequestMethod.GET)
	public ApiResponse findAll(
		@RequestParam(value="namesOnly", required=false, defaultValue="false") 
		boolean namesOnly,

		@RequestParam(value="page",		 required=false, defaultValue="0") 
		int page,

		@RequestParam(value="pageSize",  required=false, defaultValue="10") 
		int pageSize
	) {

		ApiResponse resp;
		
		try {

			if (namesOnly) {
				List<FacilityName> names = facilitiesService.findFacilities(new PageInfo(page, pageSize));
				resp = ApiResponse.createSuccessResponse(names);
			} else {
				List<Facility> facilities = facilitiesService.findAll();
				resp = ApiResponse.createSuccessResponse(facilities);
			}

		} catch (ServiceException e) {
			resp = ApiResponse.createFailedResponse(e.getErrorMessages());
		}
		
		return resp;
	}
	
	@ResponseBody
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ApiResponse findById(@PathVariable long id) {
		
		ApiResponse resp;
		
		// convert this to AOP
		try {
			Facility facility = facilitiesService.findFacilityById(id);
			resp = ApiResponse.createSuccessResponse(facility);
		} catch (ServiceException e) {
			resp = ApiResponse.createFailedResponse(e.getErrorMessages());
		}

		return resp;
	}
	
	/**
	 * Sample Data:
	 * 
	 * {
		  "facilityName": "Zek Test Fac",
		  "facilityLogo": "http://newfac.com/img.jpg",
		  "blendedRate": 0.777,
		  "medicalSystem": {
		    "medicalgroupid": 134,
		    "description": "don't delete this",
		    "systemname": "ddt"
		  },
		  "address": {
		    "addressid": 278,
		    "state_": null,
		    "street": "Matapat st.",
		    "city": "Ginoong City",
		    "zipcode": null,
		    "country": "PH"
		  },
		  "withChargeCodes": true,
		  "imageExportEnabled": true,
		  "canFinalizeOutsideOfSystem": true,
		  "active": true
		}
	 * @param fac
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="add", method=RequestMethod.POST)
	public ApiResponse add(@RequestBody Facility fac) {
		
		ApiResponse resp;
		
		try {
			Facility ret = facilitiesService.createFacility(fac, "just test".getBytes());
			resp = ApiResponse.createSuccessResponse(ret);
			resp.message = "Succesfully created facility.";
		} catch (ServiceException e) {
			resp = ApiResponse.createFailedResponse(e.getErrorMessages());
		}
		
		return resp;
	}
	
	/**
	 * Sample Data:
	 * 
	 * {
	        "facilityId": 199,
	        "facilityName": "Zek Test Fac Z",
	        "facilityLogo": "http://mykamotequeue.bucket/images/dda8e674-6a8b-453c-ae16-7f0ffe0eb25a",
	        "blendedRate": 0.8787,
	        "medicalSystem": {
	            "medicalgroupid": 134,
	            "description": "don't delete this",
	            "systemname": "ddt"
	        },
	        "address": {
	            "addressid": 319,
	            "state_": null,
	            "street": "Matapat st.",
	            "city": "Ginoong City",
	            "zipcode": 2009,
	            "country": "PH"
	        },
	        "withChargeCodes": null,
	        "imageExportEnabled": true,
	        "canFinalizeOutsideOfSystem": true,
	        "active": true
	    }
	 * @param fac
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="update", method=RequestMethod.POST)
	public ApiResponse update(@RequestBody Facility fac) {
		
		ApiResponse resp;
		
		try {
			Facility ret = facilitiesService.updateFacility(fac);
			resp = ApiResponse.createSuccessResponse(ret);
			resp.message = "Succesfully updated facility.";
		} catch (ServiceException e) {
			resp = ApiResponse.createFailedResponse(e.getErrorMessages());
		}
		
		return resp;
	}
	
	@ResponseBody
	@RequestMapping(value="delete/{id}", method=RequestMethod.POST)
	public ApiResponse delete(@PathVariable long id) {
		
		ApiResponse resp;
		
		try {
			facilitiesService.deleteFacility(id);
			resp = ApiResponse.createSuccessResponse(null);
			resp.message = "Succesfully deleted facility.";
		} catch (ServiceException e) {
			resp = ApiResponse.createFailedResponse(e.getErrorMessages());
		}
		
		return resp;
	}

}
