package com.ezekielbaniaga.cs3server.controller.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ezekielbaniaga.cs3server.controller.response.ApiResponse;
import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.logic.services.MedicalSystemsService;
import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;

@Controller
@RequestMapping("/medicalsystems")
public class MedicalSystemsEndpoint {
	
	@Autowired
	MedicalSystemsService service;
	

	@ResponseBody
	@RequestMapping(method=RequestMethod.GET)
	public ApiResponse findAll() {

		ApiResponse resp;
		
		try {
			List<MedicalSystem> medicalSystems = service.findAll();
			resp = ApiResponse.createSuccessResponse(medicalSystems);
		} catch(ServiceException e) {
			resp = ApiResponse.createFailedResponse(e.getErrorMessages());
		}
		
		return resp;
	}
}
