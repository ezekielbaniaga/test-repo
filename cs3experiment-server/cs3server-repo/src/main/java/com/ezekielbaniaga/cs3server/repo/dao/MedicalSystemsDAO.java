package com.ezekielbaniaga.cs3server.repo.dao;

import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Add;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.AddOrUpdate;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Delete;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.FindAll;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.FindOne;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Update;
import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface MedicalSystemsDAO extends
	FindAll<MedicalSystem>,
	FindOne<MedicalSystem, Long>,
	Add<MedicalSystem, Long>,
	AddOrUpdate<MedicalSystem, Long>,
	Update<MedicalSystem>,
	Delete<MedicalSystem>{
	
	boolean systemExists(String systemName) throws RepositoryException;

	MedicalSystem findByName(String systemName) throws RepositoryException;
}
