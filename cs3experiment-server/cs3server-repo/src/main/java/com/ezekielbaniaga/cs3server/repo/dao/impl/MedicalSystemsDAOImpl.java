package com.ezekielbaniaga.cs3server.repo.dao.impl;

import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Medicalsystems.MEDICALSYSTEMS;

import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;
import com.ezekielbaniaga.cs3server.repo.dao.MedicalSystemsDAO;
import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.MedicalSystemMapper;
import com.ezekielbaniaga.cs3server.repo.jooq.tables.records.MedicalsystemsRecord;

@Repository
public class MedicalSystemsDAOImpl implements MedicalSystemsDAO {

	@Autowired
	DSLContext ctx;
	
	@Autowired
	MedicalSystemMapper medicalSystemMapper;
	
	@Override
	public List<MedicalSystem> findAll() throws RepositoryException  {
		Result<Record> result = ctx.select().from(MEDICALSYSTEMS).fetch();
		return result.map(medicalSystemMapper);
	}

	@Override
	public MedicalSystem find(Long id) throws RepositoryException  {
		Record rec = ctx.select().from(MEDICALSYSTEMS).where(MEDICALSYSTEMS.MEDICALGROUPID.eq(id)).fetchOne();
		return rec != null ? rec.map(medicalSystemMapper) : null;
	}

	@Override
	public Long add(MedicalSystem obj) throws RepositoryException  {
		MedicalsystemsRecord mr = ctx.newRecord(MEDICALSYSTEMS);
		
		mr.setSystemname(obj.systemname);
		mr.setDescription(obj.description);
		mr.store();
		obj.medicalgroupid = mr.getMedicalgroupid();
		
		return mr.getMedicalgroupid();
	}

	@Override
	public void update(MedicalSystem obj) throws RepositoryException  {
		MedicalsystemsRecord mr  = ctx.fetchOne(MEDICALSYSTEMS, 
			MEDICALSYSTEMS.MEDICALGROUPID.eq(obj.medicalgroupid));

		mr.setSystemname(obj.systemname);
		mr.setDescription(obj.description);
		mr.store();
	}

	@Override
	public Long addOrUpdate(MedicalSystem obj) throws RepositoryException  {
		if (obj != null && obj.medicalgroupid != null && obj.medicalgroupid != 0L) {
			update(obj);
		} else {
			add(obj);
		}

		return obj.medicalgroupid;
	}

	@Override
	public void delete(MedicalSystem obj) throws RepositoryException  {
		ctx.delete(MEDICALSYSTEMS).where(
			MEDICALSYSTEMS.MEDICALGROUPID.eq(obj.medicalgroupid)).execute();
	}
	
	@Override
	public boolean systemExists(String systemName) throws RepositoryException  {
		Record1<Integer> res = ctx.selectOne().from(MEDICALSYSTEMS)
			.where(MEDICALSYSTEMS.SYSTEMNAME.like(systemName)).limit(1).fetchOne();
	
		return res != null && res.value1().equals(1);
	}
	
	@Override
	public MedicalSystem findByName(String systemName) throws RepositoryException  {
		Record rec = ctx.select().from(MEDICALSYSTEMS)
			.where(MEDICALSYSTEMS.SYSTEMNAME.like(systemName)).fetchOne();

		return rec != null ? rec.map(medicalSystemMapper) : null;
	}
	
}
