package com.ezekielbaniaga.cs3server.repo.dao.commoninterface;

import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface FindOne<T, ID> {
	T find(ID id) throws RepositoryException;
}
