package com.ezekielbaniaga.cs3server.repo.dao.commoninterface;

import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface Add<T, ID> {
	ID add(T obj) throws RepositoryException;
}
