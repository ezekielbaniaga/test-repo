package com.ezekielbaniaga.cs3server.repo.dao.mappers.impl;

import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities.FACILITIES;

import org.jooq.Record;

import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.FacilityMapper;

public class LazyFacilityMapper implements FacilityMapper {

	public Facility map(Record record) {
		Facility f = new Facility();

		f.facilityId 	  = record.getValue(FACILITIES.FACILITYID);
		f.facilityName    = record.getValue(FACILITIES.FACILITYNAME);
		f.facilityLogo	  = record.getValue(FACILITIES.FACILITYLOGO);
		f.active 		  = record.getValue(FACILITIES.ACTIVE);
		f.blendedRate 	  = record.getValue(FACILITIES.BLENDEDRATE);
		f.canFinalizeOutsideOfSystem 
					      = record.getValue(FACILITIES.CANFINALIZEOUTSIDEOFSYSTEM);
		f.imageExportEnabled
						  = record.getValue(FACILITIES.IMAGEEXPORTENABLED);
		f.withChargeCodes = record.getValue(FACILITIES.WITHCHARGECODES);
		
		return f;
	}
}
