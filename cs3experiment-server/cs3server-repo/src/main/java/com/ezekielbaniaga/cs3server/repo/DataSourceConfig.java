package com.ezekielbaniaga.cs3server.repo;

import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.jolbox.bonecp.BoneCPDataSource;

/**
 * This configuration will instantiate the default DSLContext.
 * 
 *     PostgreSQL + Spring Transaction + BoneCP config
 * 
 * @author Ezekiel
 */
@Configuration
@PropertySource("classpath:application.properties")
@EnableTransactionManagement
public class DataSourceConfig {

	@Autowired
	Environment env;
	
	/**
	 * Configure jOOQ datasource
	 * @return
	 */
	@Bean(destroyMethod="close")
	public BoneCPDataSource dataSource() {
		
		BoneCPDataSource dataSource = new BoneCPDataSource();
		dataSource.setDriverClass("org.postgresql.Driver");
		dataSource.setJdbcUrl(env.getProperty("db.url"));
		dataSource.setUsername(env.getProperty("db.username"));
		dataSource.setPassword(env.getProperty("db.password"));
		
		return dataSource;
	}
	
	/**
	 * Configure Spring's transaction manager to use dataSource().
	 * @return
	 */
	@Bean
	public DataSourceTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}
	
	/**
	 * Configure jOOQ's ConnectionProvider to use Spring's TransactionAwareDataSourceProxy,
	 * which can dynamically discover the transaction context.
	 * @return
	 */
	@Bean
	public TransactionAwareDataSourceProxy transactionAwareDataSource() {
		return new TransactionAwareDataSourceProxy(dataSource());
	}
	
	@Bean
	public DataSourceConnectionProvider connectionProvider() {
		return new DataSourceConnectionProvider(transactionAwareDataSource());
	}
	
	@Bean
	public ExceptionTranslator exceptionTranslator() {
		return new ExceptionTranslator();
	}

	@Bean
	public DefaultConfiguration config() {
		DefaultConfiguration config = new DefaultConfiguration();

		config.set(connectionProvider())
			  .set(new DefaultExecuteListenerProvider(exceptionTranslator()))
			  .set(SQLDialect.POSTGRES);

		return config;
	}
	
	/**
	 * Configure the DSL object, optionally overriding jOOQ Exceptions with Spring Exceptions
	 */
	@Bean
	public DefaultDSLContext dsl() {
		return new DefaultDSLContext(config());
	}
}
