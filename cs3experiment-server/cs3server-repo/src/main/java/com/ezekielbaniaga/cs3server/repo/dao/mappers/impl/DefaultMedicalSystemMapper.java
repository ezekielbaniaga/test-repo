package com.ezekielbaniaga.cs3server.repo.dao.mappers.impl;

import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Medicalsystems.MEDICALSYSTEMS;

import org.jooq.Record;

import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.MedicalSystemMapper;

public class DefaultMedicalSystemMapper implements MedicalSystemMapper {

	@Override
	public MedicalSystem map(Record record) {
		MedicalSystem m = new MedicalSystem();

		m.description 	 = record.getValue(MEDICALSYSTEMS.DESCRIPTION);
		m.medicalgroupid = record.getValue(MEDICALSYSTEMS.MEDICALGROUPID);
		m.systemname 	 = record.getValue(MEDICALSYSTEMS.SYSTEMNAME);

		return m;
	}

}
