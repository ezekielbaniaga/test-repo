package com.ezekielbaniaga.cs3server.repo.dao.impl;

import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Addresses.ADDRESSES;
import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities.FACILITIES;
import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Medicalsystems.MEDICALSYSTEMS;

import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.ezekielbaniaga.cs3server.model.custom.FacilityName;
import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.repo.dao.FacilitiesDAO;
import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.FacilityMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.FacilityNameMapper;
import com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord;

@Repository
public class FacilitiesDAOImpl implements FacilitiesDAO {

	@Autowired
	DSLContext ctx;
	
	@Autowired
	@Qualifier("eager")
	FacilityMapper facilityMapper;
	
	@Autowired
	@Qualifier("lazy")
	FacilityMapper lazyFacilityMapper; //Unused. Just a demo that we can use diff mappers
	
	@Autowired
	FacilityNameMapper facilityNameMapper;
	
	private final void pojoToRecord(Facility obj, FacilitiesRecord fr) {
		fr.setActive(obj.active);
		fr.setBlendedrate(obj.blendedRate);
		fr.setCanfinalizeoutsideofsystem(obj.canFinalizeOutsideOfSystem);
		fr.setFacilityname(obj.facilityName);
		fr.setFacilitylogo(obj.facilityLogo);
		fr.setImageexportenabled(obj.imageExportEnabled);
		fr.setWithchargecodes(obj.withChargeCodes);

		// associated records
		fr.setAddressid(obj.address.addressid);
		fr.setMedicalsystemid(obj.medicalSystem.medicalgroupid);
	}
	
	@Override
	public Facility findByName(String name) throws RepositoryException {
		Record rec = ctx.select().from(FACILITIES)
				.leftOuterJoin(ADDRESSES).using(ADDRESSES.ADDRESSID)
				.leftOuterJoin(MEDICALSYSTEMS).on(MEDICALSYSTEMS.MEDICALGROUPID.equal(FACILITIES.MEDICALSYSTEMID))
				.where(FACILITIES.FACILITYNAME.lower().like(name.toLowerCase()))
				.fetchOne();

		return rec != null ? rec.map(facilityMapper) : null;
	}
	
	@Override
	public List<Facility> findAll() throws RepositoryException  {
		Result<Record> result = ctx.select().from(FACILITIES)
			.leftOuterJoin(MEDICALSYSTEMS).on(MEDICALSYSTEMS.MEDICALGROUPID.equal(FACILITIES.MEDICALSYSTEMID))
			.leftOuterJoin(ADDRESSES).using(ADDRESSES.ADDRESSID).fetch();

		return result.map(facilityMapper);
	}
	
	@Override
	public List<FacilityName> findFacilities(int offset, int limit) throws RepositoryException {
		Result<Record> result = ctx.select().from(FACILITIES).limit(limit).offset(offset).fetch();
		
		return result.map(facilityNameMapper);
	}

	@Override
	public Facility find(Long id) throws RepositoryException  {
		Record rec = ctx.select().from(FACILITIES)
				.leftOuterJoin(ADDRESSES).using(ADDRESSES.ADDRESSID)
				.leftOuterJoin(MEDICALSYSTEMS).on(MEDICALSYSTEMS.MEDICALGROUPID.equal(FACILITIES.MEDICALSYSTEMID))
				.where(FACILITIES.FACILITYID.eq(id))
				.fetchOne();

		return rec != null ? rec.map(facilityMapper) : null;
	}
	
	@Override
	public boolean facilityExists(String name) throws RepositoryException  {
		Record1<Integer> res = ctx.selectOne().from(FACILITIES)
				.where(FACILITIES.FACILITYNAME.lower().like(name.toLowerCase()))
				.limit(1).fetchOne();

		return res != null && res.value1().equals(1);
	}
	
	@Override
	public boolean facilityExists(String name, Long ignoreId) throws RepositoryException {
		Record1<Integer> res = ctx.selectOne().from(FACILITIES)
				.where(FACILITIES.FACILITYNAME.lower().like(name.toLowerCase()))
				.and(FACILITIES.FACILITYID.notEqual(ignoreId))
				.limit(1).fetchOne();

		return res != null && res.value1().equals(1);
	}

	@Override
	public boolean exists(Long id) throws RepositoryException {
		Record1<Integer> res = ctx.selectOne().from(FACILITIES)
				.where(FACILITIES.FACILITYID.equal(id))
				.limit(1).fetchOne();

		return res != null && res.value1().equals(1);
	}

	@Override
	public Long add(Facility obj) throws RepositoryException  {
		FacilitiesRecord fr = ctx.newRecord(FACILITIES);

		pojoToRecord(obj, fr);
		fr.store();
		obj.facilityId = fr.getFacilityid();
		
		return fr.getFacilityid();
	}

	@Override
	public void update(Facility obj) throws RepositoryException  {
		FacilitiesRecord fr = ctx.fetchOne(FACILITIES, FACILITIES.FACILITYID.eq(obj.facilityId));
		pojoToRecord(obj, fr);
		fr.store();
	}

	@Override
	public Long addOrUpdate(Facility obj) throws RepositoryException  {
		if (obj != null && obj.facilityId != null && obj.facilityId != 0L) {
			update(obj);
		} else {
			add(obj);
		}

		return obj.facilityId;
	}

	@Override
	public void delete(Facility obj) throws RepositoryException  {
		ctx.delete(FACILITIES).where(FACILITIES.FACILITYID.eq(obj.facilityId)).execute();
	}

}
