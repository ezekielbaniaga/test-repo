package com.ezekielbaniaga.cs3server.repo.dao.commoninterface;

import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface Delete<T> {
	void delete(T obj) throws RepositoryException;
}
