package com.ezekielbaniaga.cs3server.repo.dao.mappers;

import org.jooq.Record;
import org.jooq.RecordMapper;

import com.ezekielbaniaga.cs3server.model.custom.FacilityName;

public interface FacilityNameMapper extends RecordMapper<Record, FacilityName> {

}
