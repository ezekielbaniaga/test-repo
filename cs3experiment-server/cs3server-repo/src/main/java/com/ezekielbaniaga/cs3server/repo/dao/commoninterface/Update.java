package com.ezekielbaniaga.cs3server.repo.dao.commoninterface;

import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface Update<T> {
	void update(T obj) throws RepositoryException;
}
