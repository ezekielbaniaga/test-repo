package com.ezekielbaniaga.cs3server.repo.dao.mappers.impl;

import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities.FACILITIES;

import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.AddressMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.FacilityMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.MedicalSystemMapper;

public class DefaultFacilityMapper implements FacilityMapper {
	
	@Autowired
	AddressMapper addressMapper;
	
	@Autowired
	MedicalSystemMapper medicalSystemMapper;

	public Facility map(Record record) {
		Facility f = new Facility();

		f.facilityId 	  = record.getValue(FACILITIES.FACILITYID);
		f.facilityName    = record.getValue(FACILITIES.FACILITYNAME);
		f.facilityLogo	  = record.getValue(FACILITIES.FACILITYLOGO);
		f.active 		  = record.getValue(FACILITIES.ACTIVE);
		f.blendedRate 	  = record.getValue(FACILITIES.BLENDEDRATE);
		f.canFinalizeOutsideOfSystem 
					      = record.getValue(FACILITIES.CANFINALIZEOUTSIDEOFSYSTEM);
		f.imageExportEnabled
						  = record.getValue(FACILITIES.IMAGEEXPORTENABLED);
		f.withChargeCodes = record.getValue(FACILITIES.WITHCHARGECODES);

		// associated record
		
		f.address 		  = addressMapper.map(record);
		f.medicalSystem   = medicalSystemMapper.map(record);

		return f;
	}
}