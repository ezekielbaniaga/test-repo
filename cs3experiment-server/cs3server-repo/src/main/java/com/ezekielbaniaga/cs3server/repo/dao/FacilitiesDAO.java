package com.ezekielbaniaga.cs3server.repo.dao;

import java.util.List;

import com.ezekielbaniaga.cs3server.model.custom.FacilityName;
import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Add;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.AddOrUpdate;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Delete;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Exists;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.FindAll;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.FindOne;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Update;
import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface FacilitiesDAO extends 
	FindAll<Facility>, 
	FindOne<Facility, Long>,
	Add<Facility, Long>,
	AddOrUpdate<Facility, Long>,
	Update<Facility>,
	Delete<Facility>,
	Exists<Long> {

	boolean facilityExists(String name) throws RepositoryException;

	boolean facilityExists(String name, Long ignoreId) throws RepositoryException;
	
	Facility findByName(String name) throws RepositoryException;
	
	List<FacilityName> findFacilities(int offset, int limit) throws RepositoryException;
}
