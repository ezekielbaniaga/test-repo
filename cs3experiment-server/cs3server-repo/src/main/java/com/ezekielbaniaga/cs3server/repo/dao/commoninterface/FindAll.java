package com.ezekielbaniaga.cs3server.repo.dao.commoninterface;

import java.util.List;

import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface FindAll<T> {
	List<T> findAll() throws RepositoryException;
}
