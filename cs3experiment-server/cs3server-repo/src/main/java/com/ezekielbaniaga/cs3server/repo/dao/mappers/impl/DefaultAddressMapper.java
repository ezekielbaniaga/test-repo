package com.ezekielbaniaga.cs3server.repo.dao.mappers.impl;

import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Addresses.ADDRESSES;

import org.jooq.Record;

import com.ezekielbaniaga.cs3server.model.entities.Address;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.AddressMapper;

public class DefaultAddressMapper implements AddressMapper {

	@Override
	public Address map(Record record) {
		Address address 	= new Address();
		address.addressid 	= record.getValue(ADDRESSES.ADDRESSID);
		address.city 		= record.getValue(ADDRESSES.CITY);
		address.country 	= record.getValue(ADDRESSES.COUNTRY);
		address.state_ 		= record.getValue(ADDRESSES.STATE_);
		address.street 		= record.getValue(ADDRESSES.STREET);
		address.zipcode 	= record.getValue(ADDRESSES.ZIPCODE);
		
		return address;
	}

}
