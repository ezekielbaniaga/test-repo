package com.ezekielbaniaga.cs3server.repo.dao.impl;

import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Addresses.ADDRESSES;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ezekielbaniaga.cs3server.model.entities.Address;
import com.ezekielbaniaga.cs3server.repo.dao.AddressesDAO;
import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.AddressMapper;
import com.ezekielbaniaga.cs3server.repo.jooq.tables.records.AddressesRecord;

@Repository
public class AddressesDAOImpl implements AddressesDAO {

	@Autowired
	DSLContext ctx;
	
	@Autowired
	AddressMapper addressesMapper;
	
	/**
	 * Used to avoid repetitive properties mapping.
	 * @param obj 
	 * @param ar A
	 */
	private final void pojoToRecord(Address obj, AddressesRecord ar) {
		ar.setCity(obj.city);
		ar.setCountry(obj.country);
		ar.setState_(obj.state_);
		ar.setStreet(obj.street);
		ar.setZipcode(obj.zipcode);
	}

	@Override
	public Address find(Long id) throws RepositoryException {
		Record rec = ctx.select().from(ADDRESSES).where(ADDRESSES.ADDRESSID.eq(id)).fetchOne();
		return rec != null ? rec.map(addressesMapper) : null;
	}

	@Override
	public Long add(Address obj) throws RepositoryException {
		AddressesRecord ar = ctx.newRecord(ADDRESSES);
		
		pojoToRecord(obj, ar);
		ar.store();
		obj.addressid = ar.getAddressid();
		
		return ar.getAddressid();
	}

	@Override
	public void update(Address obj) throws RepositoryException {
		AddressesRecord ar = ctx.fetchOne(ADDRESSES, ADDRESSES.ADDRESSID.eq(obj.addressid));
		pojoToRecord(obj, ar);
		ar.store();
	}
	
	@Override
	public Long addOrUpdate(Address obj) throws RepositoryException {
		if (obj != null && obj.addressid != null && obj.addressid != 0L) {
			update(obj);
		} else {
			add(obj);
		}

		return obj.addressid;
	}

	@Override
	public void delete(Address obj) throws RepositoryException {
		ctx.delete(ADDRESSES).where(ADDRESSES.ADDRESSID.eq(obj.addressid)).execute();
	}

	@Override
	public Address findByCityCountryState(String city, String country,
			String state) throws RepositoryException {
		Record rec = ctx.select().from(ADDRESSES)
				.where(ADDRESSES.CITY.like(city)
				.and(ADDRESSES.COUNTRY.like(country)
				.and(ADDRESSES.STATE_.like(state)))).fetchAny();
		
		return rec.map(addressesMapper);
	}
	
	@Override
	public boolean cityCountryStateExists(String city, String country,
			String state) throws RepositoryException {
		Record1<Integer> res = ctx.selectOne().from(ADDRESSES)
				.where(ADDRESSES.CITY.like(city)
				.and(ADDRESSES.COUNTRY.like(country)
				.and(ADDRESSES.STATE_.like(state)))).limit(1).fetchOne();

		return res != null && res.value1().equals(1);
	}
	
}
