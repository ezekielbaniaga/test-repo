package com.ezekielbaniaga.cs3server.repo.dao;

import com.ezekielbaniaga.cs3server.model.entities.Address;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Add;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.AddOrUpdate;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Delete;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.FindOne;
import com.ezekielbaniaga.cs3server.repo.dao.commoninterface.Update;
import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface AddressesDAO extends 
	FindOne<Address, Long>, 
	Add<Address, Long>,
	AddOrUpdate<Address, Long>,
	Update<Address>,
	Delete<Address> {

	Address findByCityCountryState(String city, String country, String state) throws RepositoryException;
	boolean cityCountryStateExists(String city, String country, String state) throws RepositoryException;
}
