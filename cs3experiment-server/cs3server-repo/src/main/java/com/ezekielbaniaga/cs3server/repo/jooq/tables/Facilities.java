/**
 * This class is generated by jOOQ
 */
package com.ezekielbaniaga.cs3server.repo.jooq.tables;

/**
 * This class is generated by jOOQ.
 */
@javax.annotation.Generated(value    = { "http://www.jooq.org", "3.2.2" },
                            comments = "This class is generated by jOOQ")
@java.lang.SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Facilities extends org.jooq.impl.TableImpl<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord> {

	private static final long serialVersionUID = 1642266184;

	/**
	 * The singleton instance of <code>public.facilities</code>
	 */
	public static final com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities FACILITIES = new com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities();

	/**
	 * The class holding records for this type
	 */
	@Override
	public java.lang.Class<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord> getRecordType() {
		return com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord.class;
	}

	/**
	 * The column <code>public.facilities.facilityid</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Long> FACILITYID = createField("facilityid", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this);

	/**
	 * The column <code>public.facilities.facilityname</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.String> FACILITYNAME = createField("facilityname", org.jooq.impl.SQLDataType.VARCHAR.length(50), this);

	/**
	 * The column <code>public.facilities.blendedrate</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Float> BLENDEDRATE = createField("blendedrate", org.jooq.impl.SQLDataType.REAL, this);

	/**
	 * The column <code>public.facilities.medicalsystemid</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Long> MEDICALSYSTEMID = createField("medicalsystemid", org.jooq.impl.SQLDataType.BIGINT, this);

	/**
	 * The column <code>public.facilities.addressid</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Long> ADDRESSID = createField("addressid", org.jooq.impl.SQLDataType.BIGINT, this);

	/**
	 * The column <code>public.facilities.withchargecodes</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Boolean> WITHCHARGECODES = createField("withchargecodes", org.jooq.impl.SQLDataType.BOOLEAN, this);

	/**
	 * The column <code>public.facilities.imageexportenabled</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Boolean> IMAGEEXPORTENABLED = createField("imageexportenabled", org.jooq.impl.SQLDataType.BOOLEAN, this);

	/**
	 * The column <code>public.facilities.canfinalizeoutsideofsystem</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Boolean> CANFINALIZEOUTSIDEOFSYSTEM = createField("canfinalizeoutsideofsystem", org.jooq.impl.SQLDataType.BOOLEAN.defaulted(true), this);

	/**
	 * The column <code>public.facilities.active</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Boolean> ACTIVE = createField("active", org.jooq.impl.SQLDataType.BOOLEAN.defaulted(true), this);

	/**
	 * The column <code>public.facilities.facilitylogo</code>. 
	 */
	public final org.jooq.TableField<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.String> FACILITYLOGO = createField("facilitylogo", org.jooq.impl.SQLDataType.VARCHAR.length(255), this);

	/**
	 * Create a <code>public.facilities</code> table reference
	 */
	public Facilities() {
		super("facilities", com.ezekielbaniaga.cs3server.repo.jooq.Public.PUBLIC);
	}

	/**
	 * Create an aliased <code>public.facilities</code> table reference
	 */
	public Facilities(java.lang.String alias) {
		super(alias, com.ezekielbaniaga.cs3server.repo.jooq.Public.PUBLIC, com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities.FACILITIES);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Identity<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, java.lang.Long> getIdentity() {
		return com.ezekielbaniaga.cs3server.repo.jooq.Keys.IDENTITY_FACILITIES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.UniqueKey<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord> getPrimaryKey() {
		return com.ezekielbaniaga.cs3server.repo.jooq.Keys.FACILITIES_PKEY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.util.List<org.jooq.UniqueKey<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord>> getKeys() {
		return java.util.Arrays.<org.jooq.UniqueKey<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord>>asList(com.ezekielbaniaga.cs3server.repo.jooq.Keys.FACILITIES_PKEY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.util.List<org.jooq.ForeignKey<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, ?>> getReferences() {
		return java.util.Arrays.<org.jooq.ForeignKey<com.ezekielbaniaga.cs3server.repo.jooq.tables.records.FacilitiesRecord, ?>>asList(com.ezekielbaniaga.cs3server.repo.jooq.Keys.FACILITIES__FACILITIES_MEDICALSYSTEMID_FKEY, com.ezekielbaniaga.cs3server.repo.jooq.Keys.FACILITIES__FACILITY_ADDRESS_FKEY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities as(java.lang.String alias) {
		return new com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities(alias);
	}
}
