package com.ezekielbaniaga.cs3server.repo.dao.commoninterface;

import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface AddOrUpdate<T, ID> {
	ID addOrUpdate(T obj) throws RepositoryException;
}
