package com.ezekielbaniaga.cs3server.repo.dao.mappers.impl;

import static com.ezekielbaniaga.cs3server.repo.jooq.tables.Facilities.FACILITIES;

import org.jooq.Record;

import com.ezekielbaniaga.cs3server.model.custom.FacilityName;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.FacilityNameMapper;

public class DefaultFacilityNameMapper implements FacilityNameMapper {

	@Override
	public FacilityName map(Record record) {
		FacilityName fn = new FacilityName();

		fn.facilityId 	  = record.getValue(FACILITIES.FACILITYID);
		fn.facilityName   = record.getValue(FACILITIES.FACILITYNAME);

		return fn;
	}

}
