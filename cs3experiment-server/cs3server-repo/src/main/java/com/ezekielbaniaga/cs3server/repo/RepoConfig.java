package com.ezekielbaniaga.cs3server.repo;

import org.springframework.aop.Advisor;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.aop.support.RegexpMethodPointcutAdvisor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.ezekielbaniaga.cs3server.repo.annotations.MapperBean;
import com.ezekielbaniaga.cs3server.repo.aop.RepoThrowsAdvice;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.AddressMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.FacilityMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.FacilityNameMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.MedicalSystemMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.impl.DefaultAddressMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.impl.DefaultFacilityMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.impl.DefaultFacilityNameMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.impl.DefaultMedicalSystemMapper;
import com.ezekielbaniaga.cs3server.repo.dao.mappers.impl.LazyFacilityMapper;

@Configuration
@ComponentScan(basePackages={"com.ezekielbaniaga.cs3server.repo.dao"})
@Import(DataSourceConfig.class)
public class RepoConfig {

	// Configure mappers
	
	@MapperBean
	@Qualifier("eager")
	public FacilityMapper defaultFacilityMapper() {
		return new DefaultFacilityMapper();
	}
	
	@MapperBean
	@Qualifier("lazy")
	public FacilityMapper lazyFacilityMapper() {
		return new LazyFacilityMapper();
	}
		
	@MapperBean
	public FacilityNameMapper facilityNameMapper() {
		return new DefaultFacilityNameMapper();
	}
	
	@MapperBean
	public AddressMapper addressMapper() {
		return new DefaultAddressMapper();
	}
	
	@MapperBean
	public MedicalSystemMapper medicalSystemMapper() {
		return new DefaultMedicalSystemMapper();
	}

	// Configure AOP
	
	@Bean
	public ThrowsAdvice throwsAdvice() {
		return new RepoThrowsAdvice();
	}

	@Bean
	public Advisor throwableAdvisor() {
		RegexpMethodPointcutAdvisor advisor = new RegexpMethodPointcutAdvisor();
		advisor.setAdvice(throwsAdvice());
		advisor.setPatterns(new String[]{".*"});
		
		return advisor;
	}
	
	@Bean
	public BeanNameAutoProxyCreator repoAutoProxy() {
		BeanNameAutoProxyCreator autoProxier = new BeanNameAutoProxyCreator();
		autoProxier.setBeanNames(new String[]{"*DAOImpl"});
		autoProxier.setInterceptorNames(new String[]{"throwableAdvisor"});
		
		return autoProxier;
	}
}
