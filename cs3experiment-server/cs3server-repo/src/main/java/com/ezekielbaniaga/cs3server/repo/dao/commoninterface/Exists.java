package com.ezekielbaniaga.cs3server.repo.dao.commoninterface;

import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public interface Exists<ID> {
	boolean exists(ID id) throws RepositoryException;
}
