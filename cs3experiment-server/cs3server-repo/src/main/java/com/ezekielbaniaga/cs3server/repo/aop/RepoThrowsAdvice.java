package com.ezekielbaniaga.cs3server.repo.aop;

import org.springframework.aop.ThrowsAdvice;

import com.ezekielbaniaga.cs3server.repo.dao.exceptions.RepositoryException;

public class RepoThrowsAdvice implements ThrowsAdvice {

	public void afterThrowing(Throwable throwable) throws Exception {
		RepositoryException rex = new RepositoryException("Error during dao execution");
		rex.initCause(throwable);
		
		throw rex;
	}
}
