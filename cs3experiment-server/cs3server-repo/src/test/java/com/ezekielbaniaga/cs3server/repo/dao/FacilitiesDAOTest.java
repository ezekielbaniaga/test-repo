package com.ezekielbaniaga.cs3server.repo.dao;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.model.entities.Address;
import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;

public class FacilitiesDAOTest extends AbstractDAOTest {

	@Autowired
	FacilitiesDAO dao;

	@Autowired
	AddressesDAO addrDao;

	@Autowired
	MedicalSystemsDAO medSysDao;
	
	@Test
	public void createRandomFacilities() {
		for (int i=0; i<=100; i++) {
			Address address = new Address(); // in here we add new record
			address.city = "Ginoong City";
			address.country = "PH";
			address.street = "Matapat st.";
			addrDao.add(address);
			
			MedicalSystem ms = medSysDao.findByName("ddt"); // here we choose an existing record
			
			if (ms == null) { // or create if missing
				ms = new MedicalSystem();
				ms.systemname = "ddt";
				ms.description = "don't delete this";
				
				medSysDao.add(ms);
			}
	
			Facility obj = new Facility();
			obj.facilityName = "My New Fac_" + i;
			obj.facilityLogo = "http://newfac.com/img.jpg";
			obj.active = true;
			obj.blendedRate = 0.2f * (float)Math.random();
			obj.canFinalizeOutsideOfSystem = Math.random() > 0.5d ? true: false;
			obj.imageExportEnabled = Math.random() > 0.5d ? true: false;
			obj.address = address;
			obj.medicalSystem = ms;
			dao.add(obj); 
		}
	}

//	@Test
	public void testAll() {
		// Let's add new record facility

		Address address = new Address(); // in here we add new record
		address.city = "Ginoong City";
		address.country = "PH";
		address.street = "Matapat st.";
		addrDao.add(address);
		
		MedicalSystem ms = medSysDao.findByName("ddt"); // here we choose an existing record
		
		if (ms == null) { // or create if missing
			ms = new MedicalSystem();
			ms.systemname = "ddt";
			ms.description = "don't delete this";
			
			medSysDao.add(ms);
		}

		Facility obj = new Facility();
		obj.facilityName = "My New Fac";
		obj.facilityLogo = "http://newfac.com/img.jpg";
		obj.active = true;
		obj.blendedRate = 0.2f;
		obj.canFinalizeOutsideOfSystem = true;
		obj.imageExportEnabled = false;
		obj.address = address;
		obj.medicalSystem = ms;
		dao.add(obj); 
		
		// Check...
		Facility obj2 = dao.find(obj.facilityId);
		assertEquals("My New Fac", obj2.facilityName);
		assertEquals("Ginoong City", obj2.address.city);
		assertEquals("ddt", obj2.medicalSystem.systemname);
		
		// Let's try the update...
		Facility obj3 = dao.find(obj.facilityId);
		obj3.facilityName = "My New Fac Modified";
		dao.update(obj3);
		
		// Check...
		Facility obj4 = dao.find(obj.facilityId);
		assertEquals("My New Fac Modified", obj4.facilityName);
		
		// Revert facility name
		Facility obj5 = dao.find(obj.facilityId);
		obj5.facilityName = "My New Fac";
		dao.update(obj5);
		
		// Find all test..
		List<Facility> facilities = dao.findAll();
		assertNotNull(facilities);
		assertTrue(facilities.size() >= 1);

		// Exists Test...
		log.info("Testing exists...");
		String facilityName =  "My New Fac";
		Long facilityId = obj5.facilityId;

		// Check...
		boolean exists = dao.facilityExists(facilityName);
		assertTrue(exists);
		
		// Check...
		exists = dao.facilityExists(facilityName, facilityId);
		assertFalse(exists);

		// Let's delete the record
		Facility obj6 = dao.find(obj.facilityId);
		dao.delete(obj6);

		// Let's delete its address too
		addrDao.delete(obj.address);

		// Check...
		Facility obj7 = dao.find(obj.facilityId);
		assertNull(obj7);
		
	}
}
