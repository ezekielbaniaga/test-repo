package com.ezekielbaniaga.cs3server.repo.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.model.entities.Address;

public class AddressesDAOTest extends AbstractDAOTest {

	@Autowired
	AddressesDAO dao;
	
	@Test
	public void testAll() {
		
		// Let's add record
		Address obj = new Address();
		obj.city = "Angeles";
		obj.country = "PH";
		obj.state_ = "Pampanga";
		obj.street = "Lakandula";
		obj.zipcode = "2009";
		
		long addressId = dao.add(obj);
		System.out.println("Addressid of angeles: " + addressId);
		
		// Check
		Address obj2 = dao.findByCityCountryState("Angeles", "PH", "Pampanga");
		assertNotNull(obj2);
		assertEquals("Angeles", obj2.city);
		
		// Let's update record
		Address obj3 = dao.findByCityCountryState("Angeles", "PH", "Pampanga");
		obj3.state_ =  "Pangasinan";
		dao.update(obj3);
		
		// Check
		Address obj4 = dao.findByCityCountryState("Angeles", "PH", "Pangasinan");
		assertNotNull(obj4);
		assertEquals("Pangasinan", obj4.state_);
		
		// Let's delete
		Address obj5 = dao.findByCityCountryState("Angeles", "PH", "Pangasinan");
		dao.delete(obj5);
		System.out.println("obj5:"+obj5.addressid);
		
		// Check
		boolean exists = dao.cityCountryStateExists("Angeles", "PH", "Pangasinan");
		assertFalse(exists);
	}
}
