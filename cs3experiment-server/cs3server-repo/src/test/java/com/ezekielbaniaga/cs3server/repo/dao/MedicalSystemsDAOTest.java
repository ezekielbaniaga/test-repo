package com.ezekielbaniaga.cs3server.repo.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;


public class MedicalSystemsDAOTest extends AbstractDAOTest {

	@Autowired
	MedicalSystemsDAO dao;

	@Test
	public void findAllTest() {
		for (MedicalSystem m : dao.findAll()) {
			System.out.println(m.systemname);
		}
	}
	
	@Test
	public void testAll() {
		boolean found;
		
		MedicalSystem obj = new MedicalSystem();
		obj.systemname = "Trial";
		obj.description = "Mong";
		
		// Let's add
		dao.add(obj);
		
		found = dao.systemExists("Trial");
		
		// Check...
		assertTrue(found);
		
		// Let's update
		String newName = String.valueOf(Math.random());
		MedicalSystem obj2 = dao.find(obj.medicalgroupid);
		obj2.systemname = newName;
		
		dao.update(obj2);
		
		// Check...
		found = dao.systemExists(newName);
		assertTrue(found);
		
		// Let's delete
		dao.delete(obj);
		
		// Check
		found = dao.systemExists(newName);
		assertFalse(found);
	}
	
	@Test
	public void testfindByName() {
		boolean found = dao.systemExists("ddt");
		
		if (found) {
			MedicalSystem ms = dao.findByName("ddt");
			assertEquals("ddt", ms.systemname);
		}
	}

}
