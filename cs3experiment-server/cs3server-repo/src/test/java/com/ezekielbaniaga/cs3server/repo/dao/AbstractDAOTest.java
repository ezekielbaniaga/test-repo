package com.ezekielbaniaga.cs3server.repo.dao;

import junit.framework.TestCase;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ezekielbaniaga.cs3server.repo.RepoConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={
	RepoConfig.class
})
public abstract class AbstractDAOTest extends TestCase {
	protected Logger log = Logger.getLogger(AbstractDAOTest.class);

	public AbstractDAOTest() {
		// we should be doing this on property file
		log.setLevel(Level.INFO);
	}
}
