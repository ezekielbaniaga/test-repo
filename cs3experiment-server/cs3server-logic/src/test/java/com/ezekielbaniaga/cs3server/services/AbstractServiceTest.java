package com.ezekielbaniaga.cs3server.services;

import junit.framework.TestCase;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ezekielbaniaga.cs3server.logic.LogicConfig;
import com.ezekielbaniaga.cs3server.repo.RepoConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={
	LogicConfig.class,
	RepoConfig.class
})
public abstract class AbstractServiceTest extends TestCase {

	protected Logger log = Logger.getLogger(AbstractServiceTest .class);

	public AbstractServiceTest() {
		log.setLevel(Level.INFO);
	}
}
