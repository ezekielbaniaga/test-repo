package com.ezekielbaniaga.cs3server.services;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.logic.services.MedicalSystemsService;
import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;

public class MedicalSystemsTest extends AbstractServiceTest {

	@Autowired
	MedicalSystemsService service;
	
	@Test
	public void testMedSys() throws Exception {
		List<MedicalSystem> meds = service.findAll();
		
		assertNotNull(meds);

		for (MedicalSystem m : meds) {
			log.info(m.systemname);
		}
	}
}
