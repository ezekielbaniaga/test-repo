package com.ezekielbaniaga.cs3server.services;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.logic.services.FacilitiesService;
import com.ezekielbaniaga.cs3server.model.entities.Address;
import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;

public class FacilitiesServiceTest extends AbstractServiceTest {

	@Autowired
	FacilitiesService service; 
	
	@Test
	public void testCreateFacility() throws Exception {
		Facility facility = new Facility();
		facility.active = true;
		facility.blendedRate = 0.5f;
		facility.canFinalizeOutsideOfSystem = true;
		facility.facilityName = "Test Facility Service";
		facility.imageExportEnabled = false;
		facility.withChargeCodes = false;

		facility.address = new Address();
		facility.address.city = "New York City";
		facility.address.country = "US";
		facility.address.street = "Elm Street";
		facility.address.zipcode = "2009";
		
		facility.medicalSystem = new MedicalSystem();
		facility.medicalSystem.systemname = "SSS";
		facility.medicalSystem.description = "Social Security System";
		
		byte[] facilityLogo = "The quick brown fox".getBytes();

		try {
			service.createFacility(facility, facilityLogo);
		} catch (ServiceException sex) {
			log.error("\n\n\n\n----------------------------------------------------------------------\n\n\n");
			log.error("Invalid:");
			log.error(StringUtils.join(sex.getErrorMessages(), "\n"));
			log.error("\n\n\n\n----------------------------------------------------------------------\n\n\n");
		} catch (Exception ex) {
			log.error("exception thrown during service call", ex);
		}

		facility = service.findFacilityByName("Test Facility Service");
		assertNotNull(facility);
	}
	
	@Test
	public void testUpdateFacility() throws Exception {
		testCreateFacility();
		
		Facility facility = service.findFacilityByName("Test Facility Service");
		facility.blendedRate = 1.0f;
		facility.address.street = "Miska Muska";
		
		try {
			service.updateFacility(facility);
		} catch (ServiceException sex) {
			log.error("\n\n\n\n----------------------------------------------------------------------\n\n\n");
			log.error("Invalid:");
			log.error(StringUtils.join(sex.getErrorMessages(), "\n"));
			log.error("\n\n\n\n----------------------------------------------------------------------\n\n\n");
		} catch (Exception ex) {
			log.error("exception thrown during service call", ex);
		}
		
		facility = service.findFacilityByName("Test Facility Service");
		assertEquals(facility.address.street, "Miska Muska");
		assertEquals(facility.blendedRate, 1.0f);
	}
	
	@Test
	public void testDeleteFacility() throws Exception {
		testCreateFacility();
		
		Facility facility = service.findFacilityByName("Test Facility Service");
		service.deleteFacility(facility.facilityId);

		facility = service.findFacilityByName("Test Facility Service");
		assertNull(facility);
	}
	
	@Test
	public void testFind() throws Exception {
		Facility facility = service.findFacilityByName("Test Facility Service");
	}

}
