package com.ezekielbaniaga.cs3server.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.logic.services.TestService;
import com.ezekielbaniaga.cs3server.model.entities.MyTestEntity;

public class ServiceTest extends AbstractServiceTest {

	@Autowired
	TestService test;

	@Test
	public void test() {
//		assertTrue(false);
		log.info("-------------------------TESTING METHOD LEVEL VALIDATION-------------------------");
		try {
			test.test("myS$$$$tring", null);
		} catch (ServiceException e) {
			
			System.out.println("Errors found:");
			for (String err : e.getErrorMessages()) {
				System.out.println(err);
			}

//			e.printStackTrace();
		}
	}
	
	@Test
	public void test2() {
		log.info("-------------------------TESTING CLASS AND FIELD LEVEL VALIDATION-------------------------");
		try {
			MyTestEntity entity = new MyTestEntity();
			entity.test1 = "Hello";
			entity.test2 =  "Hello WOrld ^^";
			test.test2(entity);
		} catch (ServiceException e) {
			
			System.out.println("Errors found:");
			for (String err : e.getErrorMessages()) {
				System.out.println(err);
			}

//			e.printStackTrace();
		}
	}
}
