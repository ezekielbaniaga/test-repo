package com.ezekielbaniaga.cs3server.logic.validators.impl;

import org.apache.commons.lang3.StringUtils;

import com.ezekielbaniaga.cs3server.model.validators.AlphaSpaceOnlyValidator;

public class AlphaSpaceOnlyValidatorImpl implements AlphaSpaceOnlyValidator {

	@Override
	public boolean isNotValid(String param) {
		return param != null && !StringUtils.isAlphaSpace(param);
	}

}
