package com.ezekielbaniaga.cs3server.logic.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.logic.services.MedicalSystemsService;
import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;
import com.ezekielbaniaga.cs3server.repo.dao.MedicalSystemsDAO;

@Service
public class MedicalSystemsServiceImpl extends AbstractService implements
		MedicalSystemsService {

	@Autowired
	MedicalSystemsDAO dao;
	
	@Override
	public List<MedicalSystem> findAll() throws ServiceException {
		return dao.findAll();
	}

}
