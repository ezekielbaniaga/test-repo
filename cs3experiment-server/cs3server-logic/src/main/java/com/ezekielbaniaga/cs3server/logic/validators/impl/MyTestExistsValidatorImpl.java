package com.ezekielbaniaga.cs3server.logic.validators.impl;

import com.ezekielbaniaga.cs3server.model.entities.MyTestEntity;
import com.ezekielbaniaga.cs3server.model.validators.MyTestEntityExistsValidator;

public class MyTestExistsValidatorImpl implements MyTestEntityExistsValidator {

	@Override
	public boolean isNotValid(MyTestEntity param) {
		if (param.test1.equals("Hello")) {
			return true;
		}

		return false;
	}

}
