package com.ezekielbaniaga.cs3server.logic;

import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.aop.support.RegexpMethodPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.ezekielbaniaga.cs3server.logic.aop.ServiceValidationInterceptor;

@Configuration
@Import(ValidatorsConfig.class)
@ComponentScan(basePackages={
	"com.ezekielbaniaga.cs3server.logic.services",
	"com.ezekielbaniaga.cs3server.logic.components",
	"com.ezekielbaniaga.cs3server.logic.validators",
	"com.ezekielbaniaga.cs3server.logic.utils"
})
public class LogicConfig {

	// Configure AOP
	
	@Bean
	MethodInterceptor interceptor() {
		return new ServiceValidationInterceptor();
	}
	
	@Bean
	Advisor validationAdvisor() {
		RegexpMethodPointcutAdvisor advisor = new RegexpMethodPointcutAdvisor();
		advisor.setAdvice(interceptor());
		advisor.setPatterns(new String[]{".*"});
		
		return advisor;
	}
	
	@Bean
	BeanNameAutoProxyCreator logicAutoProxy() {
		BeanNameAutoProxyCreator autoProxier = new BeanNameAutoProxyCreator();
		autoProxier.setBeanNames(new String[]{"*ServiceImpl"});
		autoProxier.setInterceptorNames(new String[]{"validationAdvisor"});
		
		return autoProxier;
	}

}
