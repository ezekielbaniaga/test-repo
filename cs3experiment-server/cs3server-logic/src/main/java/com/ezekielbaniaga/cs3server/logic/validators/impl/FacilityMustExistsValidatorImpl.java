package com.ezekielbaniaga.cs3server.logic.validators.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.model.validators.FacilityMustExistsValidator;
import com.ezekielbaniaga.cs3server.repo.dao.FacilitiesDAO;

public class FacilityMustExistsValidatorImpl implements
		FacilityMustExistsValidator {
	
	@Autowired
	FacilitiesDAO dao;

	@Override
	public boolean isNotValid(Long param) {
		return !dao.exists(param);
	}

}
