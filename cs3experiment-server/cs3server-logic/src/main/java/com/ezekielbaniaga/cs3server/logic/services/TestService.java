package com.ezekielbaniaga.cs3server.logic.services;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.model.entities.MyTestEntity;
import com.ezekielbaniaga.cs3server.model.validators.AlphaSpaceOnlyValidator;
import com.ezekielbaniaga.cs3server.model.validators.StringNotBlankValidator;
import com.ezekielbaniaga.cs3server.model.validators.ValidateLogic;
import com.ezekielbaniaga.cs3server.model.validators.ValidateWith;

public interface TestService {

	void test(

		@ValidateWith({
			@ValidateLogic(validator=AlphaSpaceOnlyValidator.class, message="myString1 must only contain alpha characters and/or space."),
			@ValidateLogic(validator=StringNotBlankValidator.class, message="myString1 must not be blank.")
		}) 
		String myString1, 
			
		@ValidateWith({
			@ValidateLogic(validator=StringNotBlankValidator.class, message="myString2 must not be blank.")
		}) 
		String myString2

	) throws ServiceException;
	
	void test2(MyTestEntity entity) throws ServiceException;
}
