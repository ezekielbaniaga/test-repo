package com.ezekielbaniaga.cs3server.logic.validators.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.model.validators.FacilityExistsValidator;
import com.ezekielbaniaga.cs3server.repo.dao.FacilitiesDAO;

public class FacilityExistsValidatorImpl implements FacilityExistsValidator {

	@Autowired
	FacilitiesDAO dao;
	
	@Override
	public boolean isNotValid(Facility facility) {
		if (facility.facilityId == null  || facility.facilityId == 0L) {
			return dao.facilityExists(facility.facilityName);
		} else {
			return dao.facilityExists(facility.facilityName, facility.facilityId);
		}
	}

}
