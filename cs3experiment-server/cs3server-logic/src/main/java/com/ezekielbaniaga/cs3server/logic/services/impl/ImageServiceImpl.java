package com.ezekielbaniaga.cs3server.logic.services.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.logic.services.ImageService;

@Service
public class ImageServiceImpl extends AbstractService implements ImageService {

	@Override
	@Transactional
	public String saveImage(byte[] data) throws ServiceException {
	
		// unimplemented code here.
		// save to image bucket.

		return "http://mykamotequeue.bucket/images/" + UUID.randomUUID().toString();
	}

}
