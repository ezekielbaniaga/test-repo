package com.ezekielbaniaga.cs3server.logic.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

@Retention(RetentionPolicy.RUNTIME)
@Bean
@Scope("prototype")
public @interface ValidatorBean {
}
