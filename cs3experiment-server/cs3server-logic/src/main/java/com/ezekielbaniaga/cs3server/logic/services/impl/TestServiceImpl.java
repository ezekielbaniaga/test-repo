package com.ezekielbaniaga.cs3server.logic.services.impl;

import org.springframework.stereotype.Service;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.logic.services.TestService;
import com.ezekielbaniaga.cs3server.model.entities.MyTestEntity;

@Service
public class TestServiceImpl extends AbstractService implements TestService {

	@Override
	public void test(String myString1, String myString2) throws ServiceException {
		System.out.println("Showing strings:" + myString1 + " - " + myString2);
	}

	@Override
	public void test2(MyTestEntity entity) throws ServiceException {
		System.out.println("Successfully got here..." + entity.test1);
	}
}
