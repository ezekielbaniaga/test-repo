package com.ezekielbaniaga.cs3server.logic.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.logic.services.FacilitiesService;
import com.ezekielbaniaga.cs3server.logic.services.ImageService;
import com.ezekielbaniaga.cs3server.model.custom.FacilityName;
import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.model.internal.PageInfo;
import com.ezekielbaniaga.cs3server.repo.dao.AddressesDAO;
import com.ezekielbaniaga.cs3server.repo.dao.FacilitiesDAO;
import com.ezekielbaniaga.cs3server.repo.dao.MedicalSystemsDAO;

@Service
public class FacilitiesServiceImpl extends AbstractService implements FacilitiesService {
	
	@Autowired
	FacilitiesDAO dao;
	
	@Autowired
	AddressesDAO addressDao;
	
	@Autowired
	MedicalSystemsDAO medsysDao;
	
	@Autowired
	ImageService imageService;

	@Override
	@Transactional
	public Facility createFacility(Facility facility, byte[] facilityLogo) throws ServiceException {
		
		// In this service call, the logo is optional. 
		if (facilityLogo != null) {

			// Save the image...
			// Notice no need for try catch since our service throws it.
			String imageUrl = imageService.saveImage(facilityLogo);
			
			// Successful request, we set logo
			facility.facilityLogo = imageUrl;
		}
		
		// Invoke DAO operation
		medsysDao.addOrUpdate(facility.medicalSystem);
		addressDao.add(facility.address);
		dao.add(facility);
		
		return facility;
	}
	
	@Override
	@Transactional
	public Facility updateFacility(Facility facility) throws ServiceException {
		
		medsysDao.addOrUpdate(facility.medicalSystem);
		addressDao.update(facility.address);
		dao.update(facility);
		
		return facility;
	}

	@Override
	@Transactional
	public void deleteFacility(Long facilityId) throws ServiceException {

		Facility fac = dao.find(facilityId);
		
		//we won't delete med sys just the address and facility
		addressDao.delete(fac.address);
		dao.delete(fac);
	}
	
	/**
	 * Notice we don't use transaction annotation here because
	 * we are just doing a read.
	 */
	@Override 
	public Facility findFacilityByName(String facilityName) throws ServiceException {

		return dao.findByName(facilityName);
	}
	
	@Override
	public Facility findFacilityById(Long facilityId) throws ServiceException {

		return dao.find(facilityId);
	}
	
	@Override
	public List<FacilityName> findFacilities(PageInfo pageInfo) throws ServiceException {

		pageInfo.normalize();
		
		return dao.findFacilities(pageInfo.page*pageInfo.pageSize, pageInfo.pageSize);
	}
	
	@Override
	public List<Facility> findAll() throws ServiceException {

		return dao.findAll();
	}
	
}
