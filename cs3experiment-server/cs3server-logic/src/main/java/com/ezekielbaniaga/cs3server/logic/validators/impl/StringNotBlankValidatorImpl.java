package com.ezekielbaniaga.cs3server.logic.validators.impl;

import org.apache.commons.lang3.StringUtils;

import com.ezekielbaniaga.cs3server.model.validators.StringNotBlankValidator;

public class StringNotBlankValidatorImpl implements StringNotBlankValidator {

	@Override
	public boolean isNotValid(String param) {
		return StringUtils.isBlank(param);
	}

}
