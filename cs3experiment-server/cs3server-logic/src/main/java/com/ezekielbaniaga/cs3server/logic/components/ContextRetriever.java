package com.ezekielbaniaga.cs3server.logic.components;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ContextRetriever implements ApplicationContextAware {

	ApplicationContext appContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.appContext = applicationContext;
	}
	
	public ApplicationContext getApplicationContext() {
		return appContext;
	}
}
