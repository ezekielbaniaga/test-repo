package com.ezekielbaniaga.cs3server.logic;

import org.springframework.context.annotation.Configuration;

import com.ezekielbaniaga.cs3server.logic.annotations.ValidatorBean;
import com.ezekielbaniaga.cs3server.logic.validators.impl.FacilityExistsValidatorImpl;
import com.ezekielbaniaga.cs3server.logic.validators.impl.AlphaSpaceOnlyValidatorImpl;
import com.ezekielbaniaga.cs3server.logic.validators.impl.FacilityMustExistsValidatorImpl;
import com.ezekielbaniaga.cs3server.logic.validators.impl.ImageValidatorImpl;
import com.ezekielbaniaga.cs3server.logic.validators.impl.MyTestExistsValidatorImpl;
import com.ezekielbaniaga.cs3server.logic.validators.impl.StringNotBlankValidatorImpl;
import com.ezekielbaniaga.cs3server.model.validators.AlphaSpaceOnlyValidator;
import com.ezekielbaniaga.cs3server.model.validators.FacilityExistsValidator;
import com.ezekielbaniaga.cs3server.model.validators.FacilityMustExistsValidator;
import com.ezekielbaniaga.cs3server.model.validators.ImageValidator;
import com.ezekielbaniaga.cs3server.model.validators.MyTestEntityExistsValidator;
import com.ezekielbaniaga.cs3server.model.validators.StringNotBlankValidator;

@Configuration
public class ValidatorsConfig {

	@ValidatorBean
	AlphaSpaceOnlyValidator alphaSpaceOnlyValidator() {
		return new AlphaSpaceOnlyValidatorImpl();
	}
	
	@ValidatorBean
	FacilityExistsValidator facilityExists() {
		return new FacilityExistsValidatorImpl();
	}
	
	@ValidatorBean
	ImageValidator imageValidator() {
		return new ImageValidatorImpl();
	}
	
	@ValidatorBean
	StringNotBlankValidator stringNotBlank() {
		return new StringNotBlankValidatorImpl();
	}
	
	@ValidatorBean
	MyTestEntityExistsValidator myTestExistsValidator() {
		return new MyTestExistsValidatorImpl();
	}
	
	@ValidatorBean
	FacilityMustExistsValidator facilityMustExist() {
		return new FacilityMustExistsValidatorImpl();
	}
}
