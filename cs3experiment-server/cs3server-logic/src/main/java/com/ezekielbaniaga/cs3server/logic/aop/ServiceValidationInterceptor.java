package com.ezekielbaniaga.cs3server.logic.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ezekielbaniaga.cs3server.logic.components.ContextRetriever;
import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.model.validators.ValidateLogic;
import com.ezekielbaniaga.cs3server.model.validators.ValidateWith;
import com.ezekielbaniaga.cs3server.model.validators.ValidateWithin;
import com.ezekielbaniaga.cs3server.model.validators.Validator;

					
/**
 * Validation class that throws ServiceException
 *  
 * @author Ezekiel
 */
public class ServiceValidationInterceptor implements MethodInterceptor {

	Logger log = Logger.getLogger(getClass());
	
	@Autowired
	ContextRetriever retriever;

	@Override
	@SuppressWarnings("rawtypes")
	public Object invoke(MethodInvocation invocation) throws Throwable {
		
		log.info("-----------------------------------------------------------");
		List<String> errors = new LinkedList<String>();
		
		Object[] params = invocation.getArguments();
		
		// Test the intercepted method parameter annotations
		Annotation[][] anns = invocation.getMethod().getParameterAnnotations();
		
		for (int i=0; i<anns.length; i++) {
			checkValidations(anns[i], params[i], errors);
		}
		
		// Test each parameter's class level and field level annotations
		for (Object obj : params) {

			if (obj == null) continue;
			
			Class clazz = obj.getClass();
			Annotation[] annz = clazz.getAnnotations();
			
			if (annz.length > 0) {
				checkValidations(annz, obj, errors);
			}
			
			for (Field f : clazz.getFields()) {
				Annotation[] annzz = f.getAnnotations();
				
				if (annzz.length > 0) {
					checkValidations(annzz, f.get(obj), errors);
				}
			}
		}
		
		if (errors.size() > 0) {
			throw new ServiceException(errors.toArray(new String[errors.size()]));
		}
		
		// won't proceed if exception was thrown
		Object retObj = null;
		
		try {
			retObj =  invocation.proceed();
		} catch(Exception rex) {
			if (rex instanceof ServiceException) {
				throw (ServiceException) rex; 
			} else {
				log.error("There was an error when trying to call method:" + invocation.getMethod().getName(), rex);
				throw new ServiceException("There was an internal error while calling this service", rex);
			}
		}

		return retObj;
	}
	
	@SuppressWarnings({ "rawtypes" })
	private void checkFieldClassValidation(Object obj, List<String> errors) throws Throwable {
		Class clazz = obj.getClass();
		System.out.println("checking validations on " + obj);
		Annotation[] annz = clazz.getAnnotations();
		
		if (annz.length > 0) {
			checkValidations(annz, obj, errors);
		}
		
		for (Field f : clazz.getFields()) {
			Annotation[] annzz = f.getAnnotations();
			
			if (annzz.length > 0) {
				checkValidations(annzz, f.get(obj), errors);
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void checkValidations(Annotation[] anns, Object targetObject, List<String> errorHolder) throws Throwable {
		for (Annotation ann : anns) {
			boolean isAssignable = 
				ann.annotationType().isAssignableFrom(ValidateWith.class);
			
			if (ann instanceof ValidateWithin) {
				checkFieldClassValidation(targetObject, errorHolder);
			}
			
			if (isAssignable) {
				ValidateWith vw = (ValidateWith)ann;
				
				for (ValidateLogic vlogic : vw.value()) {
					
					if (vlogic.skipValidationIfNull() && targetObject == null)
						continue;
					
					Validator bean = retriever.getApplicationContext().getBean(vlogic.validator());

					log.debug("Validator Found:" + bean.getClass().getSimpleName());
					
					if (bean.isNotValid(targetObject)) {
						errorHolder.add(vlogic.message());
					}
				}
			}
		}
	}

}
