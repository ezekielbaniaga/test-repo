package com.ezekielbaniaga.cs3server.logic.services;

import java.util.List;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.model.entities.MedicalSystem;

public interface MedicalSystemsService {

	List<MedicalSystem> findAll() throws ServiceException ;
}
