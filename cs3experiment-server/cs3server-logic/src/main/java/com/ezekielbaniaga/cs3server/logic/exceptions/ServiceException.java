package com.ezekielbaniaga.cs3server.logic.exceptions;

/**
 * General service exception.
 * Extends RuntimeException for @Transactional to work
 * when this exception occurs.
 *  
 * @author Peakielsam
 */
public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String[] errors;
	
	public ServiceException(String[] errorsMessages) {
		this.errors = errorsMessages;
	}
	
	public ServiceException(String message, Throwable throwable) {
		this.errors = new String[]{message};
		initCause(throwable);
	}
	
	public String[] getErrorMessages() {
		return this.errors;
	}

}
