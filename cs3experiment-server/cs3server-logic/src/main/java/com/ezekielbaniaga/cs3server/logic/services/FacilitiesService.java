package com.ezekielbaniaga.cs3server.logic.services;

import java.util.List;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.model.custom.FacilityName;
import com.ezekielbaniaga.cs3server.model.entities.Facility;
import com.ezekielbaniaga.cs3server.model.internal.PageInfo;
import com.ezekielbaniaga.cs3server.model.validators.FacilityMustExistsValidator;
import com.ezekielbaniaga.cs3server.model.validators.ImageValidator;
import com.ezekielbaniaga.cs3server.model.validators.StringNotBlankValidator;
import com.ezekielbaniaga.cs3server.model.validators.ValidateLogic;
import com.ezekielbaniaga.cs3server.model.validators.ValidateWith;

public interface FacilitiesService {



	Facility createFacility(

		Facility facility, 

		@ValidateWith({ 
			@ValidateLogic(validator=ImageValidator.class, message="Image data is invalid", skipValidationIfNull=true)
		}) 
		byte[] facilityLogo

	) throws ServiceException;

	
	

	Facility updateFacility(

		Facility facility

	) throws ServiceException;
	
	
	Facility findFacilityById(
			
		@ValidateWith({
			@ValidateLogic(validator=FacilityMustExistsValidator.class, message="The facility you were looking doesn't exists.")
		})
		Long facilityId
			
	) throws ServiceException;
	
	
	void deleteFacility(

		@ValidateWith({
			@ValidateLogic(validator=FacilityMustExistsValidator.class, message="The facility you were deleting doesn't exists.")
		})
		Long facilityId

	) throws ServiceException;
	
	

	Facility findFacilityByName(

		@ValidateWith({
			@ValidateLogic(validator=StringNotBlankValidator.class, message="facilityName must not be blank.")
		})
		String facilityName

	) throws ServiceException;
	

	List<FacilityName> findFacilities(
		PageInfo pageInfo
	) throws ServiceException;
	
	
	List<Facility> findAll() throws ServiceException;
}
