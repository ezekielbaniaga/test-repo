package com.ezekielbaniaga.cs3server.logic.validators.impl;

import com.ezekielbaniaga.cs3server.model.validators.ImageValidator;

public class ImageValidatorImpl implements ImageValidator {

	@Override
	public boolean isNotValid(byte[] param) {
		// More advance, e.g. format is not JPEG,GIF, or PNG
		return param == null || param.length <= 0;
	}

}
