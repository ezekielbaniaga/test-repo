package com.ezekielbaniaga.cs3server.logic.services;

import com.ezekielbaniaga.cs3server.logic.exceptions.ServiceException;
import com.ezekielbaniaga.cs3server.model.validators.ImageValidator;
import com.ezekielbaniaga.cs3server.model.validators.ValidateLogic;
import com.ezekielbaniaga.cs3server.model.validators.ValidateWith;

public interface ImageService {

	/**
	 * Reeturns URL of new image
	 * 
	 * @param data
	 * @return
	 * @throws ServiceException
	 */
	public String saveImage(

			@ValidateWith({ 
				@ValidateLogic(validator=ImageValidator.class, message="Image data is invalid")
			})
			byte[] data

	) throws ServiceException;

}
