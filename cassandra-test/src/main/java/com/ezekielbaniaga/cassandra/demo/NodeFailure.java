package com.ezekielbaniaga.cassandra.demo;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import org.kohsuke.randname.RandomNameGenerator;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;

public class NodeFailure {

	public static void main(String[] args) throws Exception {
		
		// cluster info
		Cluster cluster = Cluster.builder()
			//.withCredentials("cassandra", "cassandra")
			.addContactPoints("192.168.0.136", "192.168.0.137", "192.168.0.138", "192.168.0.139", "192.168.0.140")
			.withLoadBalancingPolicy(new DCAwareRoundRobinPolicy())
			.build();
		
		// LZ4 has the same compression ratio as Snappy but much faster.
		cluster.getConfiguration()
			.getProtocolOptions()
			.setCompression(ProtocolOptions.Compression.LZ4);
		
		// Pooling options
		cluster.getConfiguration()
			.getPoolingOptions()
					.setMaxSimultaneousRequestsPerConnectionThreshold(HostDistance.LOCAL, 128)
					.setMinSimultaneousRequestsPerConnectionThreshold(HostDistance.LOCAL, 128)
					.setMaxSimultaneousRequestsPerConnectionThreshold(HostDistance.REMOTE, 128)
					.setMinSimultaneousRequestsPerConnectionThreshold(HostDistance.REMOTE, 128);
		
		Metadata metadata = cluster.getMetadata();

		System.out.println("Connected to cluster:" + metadata.getClusterName());
		
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datacenter: %s, Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack());
		}
		try {
			// session init
			Session session = cluster.connect();
			session.execute("USE persons;");
			
	
//			insertSampleData(session);
			
			verifyCount(session);

//			cleanRows(session);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(0);
		}
		
		cluster.close();
	}
	
	public static void cleanRows(final Session session) {
		final PreparedStatement psDel = session.prepare("delete from person where id = ?");

		String cql = "select id from person limit 3;";
		ResultSet rs = session.execute(cql);
		
		BoundStatement bound;

		Row row;
		int cnt=0;
		while ((row = rs.one()) != null) {
			UUID id = row.getUUID(0);
			bound = new BoundStatement(psDel);
			bound.bind(id);
			session.execute(bound);

			cnt++;
		}
		System.out.println("Deleted " + cnt + " items.");
	}
	
	public static void insertSampleData(final Session session) {
		final RandomNameGenerator r = new RandomNameGenerator((int)(1000*Math.random()));
		String cql = "insert into person(id, name, age) values (?, ?, ?);";
		final PreparedStatement psWrite = session.prepare(cql);
		psWrite.setConsistencyLevel(ConsistencyLevel.ONE);
		
    	final CountDownLatch latch = new CountDownLatch(10);
    	
    	for (int j=1; j<=10; j++) {
    		final int _j = j;
    		
	    	new Thread() {
	    		public void run() {
					BoundStatement bound;
	
					for (int i=1; i<=10000; i++) {
						bound = new BoundStatement(psWrite);
			
						String newName = r.next();
						UUID uuid = UUID.randomUUID();
						int newAge = (int)(Math.random()*50);
			
						bound.bind(
							uuid,
							newName,
							newAge
						);
						session.execute(bound);
						
						System.out.println("Writing Thread (" + _j + "): "  + i);
					}
					
					latch.countDown();
	    		}
	    	}.start();
    	}

    	try {
	    	latch.await();
    	} catch (Exception ex){ }
    	
		System.out.println("Done");
	}
	
	public static void verifyCount(Session session) {
		String cql = "select * from person;";
		PreparedStatement psRead = session.prepare(cql);
		psRead.setConsistencyLevel(ConsistencyLevel.ONE);

		BoundStatement bound = new BoundStatement(psRead);
		ResultSet rs = session.execute(bound);
		
		Row row;
		int cnt=0;
		while ((row = rs.one()) != null) {
			cnt++;
		}

		System.out.println("COUNT:" + cnt); 
	}
	
}
