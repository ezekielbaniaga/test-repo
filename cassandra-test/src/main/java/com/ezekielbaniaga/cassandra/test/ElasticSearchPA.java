package com.ezekielbaniaga.cassandra.test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class ElasticSearchPA {

	/**
	 * HTTP PUT to ES http://localhost:9200/customer/external/1
	 * 
	 * @param index
	 * @param type
	 * @param id
	 * @param data
	 */
	public void add(String index, String type, String id, String data) {
		try {
			URL url = new URL("http://192.168.0.247:9200/" + index + "/" + type
					+ "/" + id);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setUseCaches(false);
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");
			conn.connect();

			conn.getOutputStream().write(data.getBytes());

			System.out.println(conn.getResponseCode());
			System.out.println(conn.getResponseMessage());
			conn.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//
	public static void main(String[] args) {
		ElasticSearchPA pa = new ElasticSearchPA();
		pa.add("persons","student","101","{prefix: \"m\", id: 218, age: 17, name: \"monthly_emergency\"}");
//		pa.add("persons","student","101","{prefix: m, id: 838, age: 36, name: modern_trouble}");
		
	}
}
