package com.ezekielbaniaga.cassandra.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * Hello world!
 *
 */
public class App {

    public static void main( String[] args ) {
    	final SimpleClient client = new SimpleClient();
//    	client.connect("192.168.0.141","192.168.0.142");
    	client.connect("192.168.0.136", "192.168.0.137","192.168.0.138","192.168.0.139","192.168.0.140");
    	
    	// Add user
    	
//    	Set<String> emails = new HashSet<String>();
//    	emails.add("mytest@tester.com");
//    	emails.add("hello@hotmail.com");
//    	
//    	Map<Integer, String> priorities = new HashMap<Integer, String>();
//    	priorities.put(1, "eat");
//    	priorities.put(2, "sleep");
//    	priorities.put(3, "code");
//    	
//    	List<String> tags = new ArrayList<String>();
//    	tags.add("test");
//    	tags.add("make");
//    	tags.add("qa");
//    	tags.add("tester");
//
//    	client.addNewUser("mytest", "My Test", emails, priorities, tags);
    	
//    	client.millionRowTest();
//    	client.wildcardTest();

//    	client.millionRowTest3();
//    	client.countTest();

    	//-------------------------------------------------------
    	
    	//first attempt, 402ms for 10,001 records. estimated 40sec for 1m
    	//2nd attempt(cached), 235ms for 10,001 records. estimated 23.5sec for 1m
    	//optimized table. 1933ms for 120k records. 16sec for 1m
    	//read non-clustered row. 50 threads reading 1 record. Avg 18.8 ms
    	//read non-clustered row. 1070 threads reading 1 record. Avg 106.5ms
    	//read non-clustered row. 1070 threads reading 10 record. Avg 1908.6ms

//    	client.prepareRead();
//    	final CountDownLatch latch = new CountDownLatch(1070);
    	
//    	for (int i=0; i<1070; i++) {
//	    	new Thread() {
//	    		public void run() {
//			    	client.sashaReadTest(); 
//			    	latch.countDown();
//	    		}
//	    	}.start();
//    	}

//    	try {
//	    	latch.await();
//    	} catch (Exception ex){ }
    	
//    	System.out.println("Done awaiting...");
    	
    	//-------------------------------------------------------
    	//non-async, 1 thread. Done in 23297 ms or 26k records a min. estimated 38min for 1m
    	//non-async, 1 thread. Done in 18015 ms or 33k records a min. non-clustered row. 30min for 1m
    	//non-async, 107 thread, write 1 rec.  Avg 7.8ms non-clustered row. 
    	//non-async, 1070 thread, write 1 rec. Avg 52.3ms non-clustered row.
    	//non-async, 1070 thread, write 10 rec. Avg 10489.7ms non-clustered row.
    	//async, 1 thread. Done in 1873 ms. or 320,341 records a min. estimated 3.12min for 1m
    	
    	//non-async, erronous. optimized table.Done in 157261 ms for 100,000 records. 26min for 1m
    	//I noticed, di agad nawiwrite... hintay ng konti kpg hindi tama yung row count :)
    	client.prepareWrite();
    	final CountDownLatch latch = new CountDownLatch(1070);
    	for (int i=0; i<1070; i++) {
	    	new Thread() {
	    		public void run() {
			    	client.sashaInsertTest();
			    	latch.countDown();
	    		}
	    	}.start();
    	}

    	try {
	    	latch.await();
    	} catch (Exception ex){ }
    	
    	System.out.println("Done awaiting...");

    	client.close();
    }

}
