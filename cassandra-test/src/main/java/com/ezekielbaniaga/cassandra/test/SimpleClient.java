package com.ezekielbaniaga.cassandra.test;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.jboss.netty.handler.codec.socks.SocksMessage.ProtocolVersion;
import org.kohsuke.randname.RandomNameGenerator;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;

public class SimpleClient {

	private Cluster cluster;
	private Session session;
	
	public void connect(String... nodes) {
		// cluster info
		cluster = Cluster.builder()
			.withCredentials("cassandra", "cassandra")
			.addContactPoints(nodes)
			.withLoadBalancingPolicy(new DCAwareRoundRobinPolicy())
			.build();
		
		// LZ4 has the same compression ratio as Snappy but much faster.
		cluster.getConfiguration()
			.getProtocolOptions()
			.setCompression(ProtocolOptions.Compression.LZ4);
		
		// Pooling options
		cluster.getConfiguration()
			.getPoolingOptions()
					.setMaxSimultaneousRequestsPerConnectionThreshold(HostDistance.LOCAL, 128)
					.setMinSimultaneousRequestsPerConnectionThreshold(HostDistance.LOCAL, 128)
					.setMaxSimultaneousRequestsPerConnectionThreshold(HostDistance.REMOTE, 128)
					.setMinSimultaneousRequestsPerConnectionThreshold(HostDistance.REMOTE, 128);
		
		Metadata metadata = cluster.getMetadata();

		System.out.println("Connected to cluster:" + metadata.getClusterName());
		
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datacenter: %s, Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack());
		}
		
		// session init
		session = cluster.connect();
	}
	
	public void addNewUser(
		String username, // partition key
		String name, 
		Set<String> emails, 
		Map<Integer, String> priorities, 
		List<String> tags) {
		
		session.execute("USE zekspace;");
		
		long startTime = System.nanoTime();
		
		/* with collection
		PreparedStatement ps = session.prepare("INSERT INTO users(username, name, emails, priorities, tags) VALUES(?,?,?,?,?);");
		BoundStatement bound = new BoundStatement(ps);
		bound.bind(
			username,
			name,
			emails,
			priorities,
			tags
		);
		*/

		/* 10000 row test 
		PreparedStatement ps = session.prepare("INSERT INTO users(username, name) VALUES(?,?);");
		ps.setConsistencyLevel(ConsistencyLevel.ANY);
		
		for (int i=1; i<=10000; i++) { // first trial record: Done in 32699 ms(synchronous), 19162(CONSISTENCY=ONE), 17403(CONSISTENCY=ANY) and only 9226 ms (async). 
			BoundStatement bound = new BoundStatement(ps);
			bound.bind(
				username+"_"+i,
				name
			);
	
//			session.executeAsync(bound); //faster for non-critical writes i.e. no need to check if successful. e.g. user activity
			session.execute(bound);
		}*/
		
		/* 10000 row test in batch 
		PreparedStatement ps = session.prepare("INSERT INTO users(username, name) VALUES(?,?);");
		
		BatchStatement bs = new BatchStatement();
		bs.setConsistencyLevel(ConsistencyLevel.ONE);
		
		for (int i=1; i<=10000; i++) { // first trial record: Done in 4126 ms, 1775ms, 1878ms. 2583ms,1535ms(CONSISTENCY=ONE), 2517ms(CONSISTENCY=ANY), 274ms (async) 239ms(async).

			bs.add(ps.bind(
				username+"_"+i,
				name
			));
			
			if (i%500 == 0) {
//				System.out.println("Executing 500 batch"); // batch of 10k is erronous.
				session.executeAsync(bs);
				bs.clear();
			}
		}*/

		
		/* 10K Row read*/
		ResultSet rs = session.execute("SELECT * from users;");

		Row row;
		int cnt=0;
		while ((row = rs.one()) != null) {
//			System.out.println(row.getString("username"));
			cnt++;
		}
		System.out.println("COUNT:" + cnt); // first trial: Done in 242ms.

		/*
		ResultSet rs = session.execute("SELECT name from users WHERE username = 'mytest_9000';");
		System.out.println(rs.one().getString(0)); // first trial record: Done in 5 ms, 5 ms, 6 ms, 5 ms
		*/

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - startTime) / 1000000);
	}

	public void millionRowTest() {
		session.execute("USE stargatespace;");

	    RandomNameGenerator r = new RandomNameGenerator(0);

		long ignoreTime = 0,ignoreTimeLocal;
		long startTime = 0;
		
		startTime = System.nanoTime();

		PreparedStatement ps = session.prepare("INSERT INTO persons(id, name, age) VALUES(?,?,?);");
		
		BatchStatement bs = new BatchStatement();
		bs.setConsistencyLevel(ConsistencyLevel.ONE);
		
		for (int i=1; i<=1700000; i++) { // 1.7million rows 
			
			// don't benchmark name generator and age generator
			ignoreTimeLocal = System.nanoTime();
			String newName = r.next();
			int newAge = (int)(Math.random()*50);
			ignoreTime += (System.nanoTime()-ignoreTimeLocal);

			bs.add(ps.bind(
				i,
				newName,
				newAge
			));
			
			if (i%500 == 0) {
				session.executeAsync(bs);
				bs.clear();
			}
		}

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - ignoreTime - startTime) / 1000000);
	}

	public void millionRowTest2() {
		session.execute("USE stargatespace;");

	    RandomNameGenerator r = new RandomNameGenerator(0);

		long ignoreTime = 0,ignoreTimeLocal;
		long startTime = 0;
		
		startTime = System.nanoTime();

		PreparedStatement ps = session.prepare("INSERT INTO persons2(prefix, id, name, age) VALUES(?,?,?,?);");
		
		BatchStatement bs = new BatchStatement();
		bs.setConsistencyLevel(ConsistencyLevel.ANY);
		
		for (int i=1; i<=1700000; i++) { // 1.7million rows 
			
			// don't benchmark name generator and age generator
			ignoreTimeLocal = System.nanoTime();
			String newName = r.next();
			String prefix = newName.substring(0,1);
			int newAge = (int)(Math.random()*50);
			ignoreTime += (System.nanoTime()-ignoreTimeLocal);

			bs.add(ps.bind(
				prefix,
				i,
				newName,
				newAge
			));
			
			if (i%500 == 0) {
				session.executeAsync(bs);
				bs.clear();
			}
		}

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - ignoreTime - startTime) / 1000000);
	}
	
	public void millionRowTest3() {
		session.execute("USE personsspace;");

	    RandomNameGenerator r = new RandomNameGenerator(0);

		long ignoreTime = 0,ignoreTimeLocal;
		long startTime = 0;
		
		startTime = System.nanoTime();

		PreparedStatement ps = session.prepare("INSERT INTO persons(prefix, id, name, age) VALUES(?,?,?,?);");
		
		BatchStatement bs = new BatchStatement();
		bs.setConsistencyLevel(ConsistencyLevel.TWO);
		
		System.out.println("writting...1 to 50");
		for (int i=1; i<=1700000; i++) { // 1.7million rows 
			
			// don't benchmark name generator and age generator
			ignoreTimeLocal = System.nanoTime();
			String newName = r.next();
			String prefix = newName.substring(0,1);
			int newAge = (int)(Math.random()*50);
			ignoreTime += (System.nanoTime()-ignoreTimeLocal);

			bs.add(ps.bind(
				prefix,
				i,
				newName,
				newAge
			));
			
			if (i%50 == 0) {
				session.execute(bs);
				bs.clear();
				System.out.println("writting..." + i + " to " + (i+50));
			}
		}

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - ignoreTime - startTime) / 1000000);
	}

	public void wildcardTest() {
		long startTime = 0;
		session.execute("USE stargatespace;");
		startTime = System.nanoTime();
		
		String cql = "select * from persons where stargate = '{filter:{type:\"prefix\",field:\"name\",value:\"equal\"}}' limit 500;";
		ResultSet rs = session.execute(cql);

		Row row;
		int cnt=0;
		while ((row = rs.one()) != null) {
			System.out.println(row.getString("name"));
			cnt++;
		}

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - startTime) / 1000000);

		System.out.println("COUNT:" + cnt); 
	}

	public void wildcardTest2() {
		long startTime = 0;
		session.execute("USE stargatespace;");
		startTime = System.nanoTime();
		
		String cql = "select name from persons2 where stargate = '{filter:{type:\"wildcard\",field:\"name\",value:\"e*\"}}' limit 100;";
		ResultSet rs = session.execute(cql);

		Row row;
		int cnt=0;
		while ((row = rs.one()) != null) {
			System.out.println(row.getString("name"));
			cnt++;
		}

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - startTime) / 1000000);

		System.out.println("COUNT:" + cnt); 
	}

	public void countTest() {
		long startTime = 0;
		session.execute("USE persons;");
		startTime = System.nanoTime();
		
		String cql = "select * from person;";
		ResultSet rs = session.execute(cql);

		Row row;
		int cnt=0;
		while ((row = rs.one()) != null) {
//			System.out.println(row.getString("name"));
			cnt++;
		}

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - startTime) / 1000000);

		System.out.println("COUNT:" + cnt); 
	}


	PreparedStatement psRead, psWrite;
	public void prepareRead() {
		session.execute("USE persons;");
//		String cql = "select * from person where id = 8c9ce529-2cf4-43b8-8b28-7084e35aa21d";
		String cql = "select * from person;";
		psRead = session.prepare(cql);
		psRead.setConsistencyLevel(ConsistencyLevel.LOCAL_ONE);
	}
	
	public void prepareWrite() {
		session.execute("USE persons;");
		String cql = "insert into person(id, name, age) values (?, ?, ?);";
		psWrite = session.prepare(cql);
		psWrite.setConsistencyLevel(ConsistencyLevel.ANY);
	}
	
	public void sashaReadTest() {
		long startTime = 0;
		startTime = System.nanoTime();
	
		ResultSet rs = session.execute(psRead.bind());

//		Row row;
//		row = rs.one();
		int cnt=0;
		StringBuilder builder = new StringBuilder();
//		while ((row = rs.one()) != null) {
		for (Row row : rs.all()) { // memory intensive
			builder.append(row.getString("name")).append("\n");
			cnt++;
		}
		
//		System.out.println(builder.toString());

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - startTime) / 1000000);
//		System.out.printf("%d\n", (System.nanoTime() - startTime) / 1000000);

//		System.out.println(row.getString("name"));

		System.out.println("COUNT:" + cnt); 
	}

	public void sashaInsertTest() {
		long startTime = 0;
		long ignoreTime = 0,ignoreTimeLocal;
		RandomNameGenerator r = new RandomNameGenerator((int)(1000*Math.random()));

		
		BoundStatement bound;
		for (int i=1; i<=10; i++) { 
			bound = new BoundStatement(psWrite);

			// don't benchmark name generator and age generator
			ignoreTimeLocal = System.nanoTime();
			String newName = r.next();
			UUID uuid = UUID.randomUUID();
			int newAge = (int)(Math.random()*50);
			ignoreTime += (System.nanoTime()-ignoreTimeLocal);

			startTime = System.nanoTime();
			bound.bind(
				uuid,
				newName,
				newAge
			);
//			session.executeAsync(bound); //faster for non-critical writes i.e. no need to check if successful. e.g. user activity
			session.execute(bound);

			System.out.printf("%d\n", (System.nanoTime() - startTime) / 1000000);
		}

//		System.out.printf("Done in %d ms.\n", (System.nanoTime() - ignoreTime - startTime) / 1000000);
//		System.out.printf("%d\n", ms);
	}
	
	public void shashaInsertBatchTest() {
		session.execute("USE personsspace;");

	    RandomNameGenerator r = new RandomNameGenerator(0);

		long ignoreTime = 0,ignoreTimeLocal;
		long startTime = 0;
		
		startTime = System.nanoTime();

		PreparedStatement ps = session.prepare("INSERT INTO persons(prefix, id, name, age) VALUES(?,?,?,?);");
		
		BatchStatement bs = new BatchStatement();
		bs.setConsistencyLevel(ConsistencyLevel.TWO);
		
		System.out.println("writting...1 to 50");
		for (int i=1; i<=1700000; i++) { // 1.7million rows 
			
			// don't benchmark name generator and age generator
			ignoreTimeLocal = System.nanoTime();
			String newName = r.next();
			String prefix = newName.substring(0,1);
			int newAge = (int)(Math.random()*50);
			ignoreTime += (System.nanoTime()-ignoreTimeLocal);

			bs.add(ps.bind(
				prefix,
				i,
				newName,
				newAge
			));
			
			if (i%50 == 0) {
				session.execute(bs);
				bs.clear();
				System.out.println("writting..." + i + " to " + (i+50));
			}
		}

		System.out.printf("Done in %d ms.\n", (System.nanoTime() - ignoreTime - startTime) / 1000000);
	}

	public void close() {
//		cluster.closeAsync();
		cluster.close();
	}
}
