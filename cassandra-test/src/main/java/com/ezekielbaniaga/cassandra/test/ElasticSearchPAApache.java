package com.ezekielbaniaga.cassandra.test;

import java.io.ByteArrayInputStream;
import java.net.Socket;

import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.DefaultBHttpClientConnection;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpProcessorBuilder;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestExpectContinue;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.RequestUserAgent;
import org.apache.http.util.EntityUtils;

public class ElasticSearchPAApache {

	public static void main(String[] args) throws Exception {
		
			HttpProcessor httpproc = HttpProcessorBuilder.create()
	            .add(new RequestContent())
	            .add(new RequestTargetHost())
	            .add(new RequestConnControl())
	            .add(new RequestUserAgent("Test/1.1"))
	            .add(new RequestExpectContinue(true)).build();

	        HttpRequestExecutor httpexecutor = new HttpRequestExecutor();

	        HttpCoreContext coreContext = HttpCoreContext.create();
	        HttpHost host = new HttpHost("192.168.0.247", 9200);
	        coreContext.setTargetHost(host);

	        DefaultBHttpClientConnection conn = new DefaultBHttpClientConnection(8 * 1024);
	        ConnectionReuseStrategy connStrategy = DefaultConnectionReuseStrategy.INSTANCE;

	        try {

	            HttpEntity[] requestBodies = {
	                    new InputStreamEntity(
	                            new ByteArrayInputStream("{prefix: \"m\", id: 218, age: 18, name: \"monthly_emergency\"}".getBytes("UTF-8")),
	                            ContentType.APPLICATION_JSON),
	                            
	                    new InputStreamEntity(
	                            new ByteArrayInputStream("{prefix: \"m\", id: 219, age: 19, name: \"monthly_cost\"}".getBytes("UTF-8")),
	                            ContentType.APPLICATION_JSON),
	                            
	                    new InputStreamEntity(
	                            new ByteArrayInputStream("{prefix: \"m\", id: 220, age: 12, name: \"monthly_bills\"}".getBytes("UTF-8")),
	                            ContentType.APPLICATION_JSON)
	            };

	            int[] ids = {218, 219, 220};
	            for (int i = 0; i < requestBodies.length; i++) {
	                if (!conn.isOpen()) {
	                	System.out.println("host:"+host.getHostName() + ", port:" + host.getPort());
	                    Socket socket = new Socket(host.getHostName(), host.getPort());
	                    conn.bind(socket);
	                }
	                BasicHttpEntityEnclosingRequest request = new BasicHttpEntityEnclosingRequest("POST", String.format("/persons/student/%d/", ids[i]));
	                request.setEntity(requestBodies[i]);
	                System.out.println(">> Request URI: " + request.getRequestLine().getUri());

	                httpexecutor.preProcess(request, httpproc, coreContext);
	                HttpResponse response = httpexecutor.execute(request, conn, coreContext);
	                httpexecutor.postProcess(response, httpproc, coreContext);

	                System.out.println("<< Response: " + response.getStatusLine());
	                System.out.println(EntityUtils.toString(response.getEntity()));
	                System.out.println("==============");
	                if (!connStrategy.keepAlive(response, coreContext)) {
	                    conn.close();
	                } else {
	                    System.out.println("Connection kept alive...");
	                }
	            }
	        } finally {
	            conn.close();
	        }
	}
}
