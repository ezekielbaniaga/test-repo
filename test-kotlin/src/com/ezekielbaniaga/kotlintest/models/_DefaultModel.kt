package com.ezekielbaniaga.kotlintest.models

import com.ezekielbaniaga.kotlintest.repositories.Model

/**
 * Created by Peakielsam on 6/28/2016.
 */
interface _DefaultModel : Model {
    override fun asJSON(): String {
        return this.toString() // Not a real JSON for brevity
    }

    override fun asXML(): String {
        return "<person>${this.toString()}</person>"
    }
}
