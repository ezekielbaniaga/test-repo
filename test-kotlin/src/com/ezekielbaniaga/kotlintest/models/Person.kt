package com.ezekielbaniaga.kotlintest.models

/**
 * Created by Peakielsam on 6/28/2016.
 */
data class Person (
    val lastName: String,
    val firstName: String,
    val middleName: String = "",
    val age: Int = 0,
    val gender: Char = 'M'
): _DefaultModel