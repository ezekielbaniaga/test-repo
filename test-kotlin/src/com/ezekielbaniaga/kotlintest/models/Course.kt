package com.ezekielbaniaga.kotlintest.models

/**
 * Created by Peakielsam on 6/28/2016.
 */
data class Course (
    val subjects : String
): _DefaultModel