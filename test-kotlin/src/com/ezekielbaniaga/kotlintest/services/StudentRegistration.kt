package com.ezekielbaniaga.kotlintest.services

import com.ezekielbaniaga.kotlintest.models.Course
import com.ezekielbaniaga.kotlintest.models.Person
import com.ezekielbaniaga.kotlintest.repositories.getRepository

/**
 * Created by Peakielsam on 6/28/2016.
 */

fun registerStudent(p: Person, c: Course) {
    val repo = getRepository()

    repo.add(p)

    repo.add(c)

}
