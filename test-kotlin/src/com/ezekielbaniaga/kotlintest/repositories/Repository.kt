package com.ezekielbaniaga.kotlintest.repositories

/**
 * Created by Peakielsam on 6/28/2016.
 */

/**
 * Repository Header
 */
interface  Model {
    fun asJSON() : String
    fun asXML() : String
}

interface Repository {
    fun add( m: Model)
    fun update( m: Model)
    fun delete( m: Model)
}

/**
 * Repository Instance Management
 */
private val tl = RepositoryThreadLocal();

fun getRepository() : Repository {
    return tl.get()
}

class RepositoryThreadLocal : ThreadLocal<Repository>() {
    override fun initialValue(): Repository? {
        return newRepository()
    }
}