package com.ezekielbaniaga.kotlintest.repositories

/**
 * Created by Peakielsam on 6/28/2016.
 */


class DefaultRepository : Repository {

    internal constructor()

    init {
        println("Im instance: " + hashCode())
    }

    override fun add(m: Model) {
        println("---")
        println("Adding To Database: ${m.asJSON()}")
        println("---")
    }

    override fun update(m: Model) {
        println("---")
        println("Updating To Database: ${m.asJSON()}")
        println("---")
    }

    override fun delete(m: Model) {
        println("---")
        println("Deleting To Database: ${m.asJSON()}")
        println("---")
    }
}