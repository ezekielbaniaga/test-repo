package com.ezekielbaniaga.kotlintest.ui

import com.ezekielbaniaga.kotlintest.models.Course
import com.ezekielbaniaga.kotlintest.models.Person
import com.ezekielbaniaga.kotlintest.services.registerStudent
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

/**
 * Created by Peakielsam on 6/18/2016.
 */

class MainApp : Application() {

    override fun init() {
        println("Inside init() method")

        println("Testing Service")
        val p = Person(lastName = "Baniaga", firstName = "Ezekiel", age = 29)
        val c = Course(subjects = "Algebra, ComProg, Basic English Grammar")
        registerStudent(p, c)
    }

    override fun start(stage: Stage?) {
        stage?.title = "Student Record System"

        val scene = Scene(MainPanel(), 800.0, 600.0)
        addStyle(scene)
        stage?.scene = scene

        stage?.show()
    }

    override fun stop() {
        print("inside stop() method")
    }

    private fun addStyle(scene: Scene) {
        scene.stylesheets.add("/com/ezekielbaniaga/kotlintest/stylesheet.css")
    }
}