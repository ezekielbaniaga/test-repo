package com.ezekielbaniaga.kotlintest.ui.utils

import javafx.fxml.FXMLLoader
import javafx.scene.Parent

/**
 * Created by Peakielsam on 6/24/2016.
 */

private val PATH_TO_FXMLS : String = "/com/ezekielbaniaga/kotlintest/ui/fxmls/"

interface LoadingFXMLMixin {

    fun loadFXML(fname: String): Parent {
        val url = this.javaClass.getResource( PATH_TO_FXMLS + fname)
        val loader: FXMLLoader = FXMLLoader(url)

        loader.setRoot(this)
        loader.setController(this)

        return loader.load<Parent>()
    }
}

