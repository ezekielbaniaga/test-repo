package com.ezekielbaniaga.kotlintest.ui

import com.ezekielbaniaga.kotlintest.ui.utils.LoadingFXMLMixin
import javafx.fxml.FXML
import javafx.scene.layout.BorderPane


/**
 * Created by Peakielsam on 6/21/2016.
 */

/**
 * Controller
 */
class MainPanelController {

}

/**
 * View
 */
class MainPanel : BorderPane() , LoadingFXMLMixin {

    @FXML   private var mainContainer     :BorderPane?      = null
            private val contentPager      :ContentPager     = ContentPager()
            private val basicInfo         :BasicInfo        = BasicInfo()

    // Kotlin's initializer block. Don't be confused with Java's static initializer
    init {
        loadFXML("Main.fxml")
    }

    @FXML
    protected fun initialize() {
        println("JavaFX initialize main panel")

        mainContainer?.let {
            it.center = contentPager
            println("Added content pager to center")
        }
    }


}