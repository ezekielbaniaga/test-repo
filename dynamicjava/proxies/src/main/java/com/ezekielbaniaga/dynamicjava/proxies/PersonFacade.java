package com.ezekielbaniaga.dynamicjava.proxies;

public interface PersonFacade {

	public void theMethod();
	
	@RestPath(url="/persons/")
	public String findAll();
}
