package com.ezekielbaniaga.dynamicjava.proxies;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class FacadeFactory {
	
	@SuppressWarnings("unchecked")
	public static <T> T createFacade(Class<T> clazz) {
		return (T) Proxy.newProxyInstance(App.class.getClassLoader(), new Class[]{clazz}, new FacadeInvocationHandler());
	}
	
	private static class FacadeInvocationHandler implements InvocationHandler {

		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			System.out.println("Method name is: " + method.getName());
			
			if (method.isAnnotationPresent(RestPath.class)) {
				RestPath restPathAnn = method.getAnnotation(RestPath.class);
				System.out.println("This method has @RestPath annotation with URL property of: " + restPathAnn.url());
			}

			return null;
		}

	}
}
