package com.ezekielbaniaga.dynamicjava.proxies;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RestPath {
	
	/**
	 * url kunwari
	 * @return
	 */
	String url();
}
