package com.ezekielbaniaga.dynamicjava.proxies;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	PersonFacade f = FacadeFactory.createFacade(PersonFacade.class);

    	f.theMethod();
    	f.findAll();
    }
}
