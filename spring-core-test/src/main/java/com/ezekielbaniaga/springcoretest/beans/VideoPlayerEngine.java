package com.ezekielbaniaga.springcoretest.beans;

public interface VideoPlayerEngine extends MediaEngine {

	public void playAVI(String path);
}
