package com.ezekielbaniaga.springcoretest.beans.impl;

import com.ezekielbaniaga.springcoretest.beans.VideoPlayerEngine;

public class YouTubeVideoPlayer implements VideoPlayerEngine {

	private float aspectRatio;
	
	public YouTubeVideoPlayer(float aspectRatio) {
		this.aspectRatio = aspectRatio;
	}
	
	public String getEngineName() {
		return "YouTube 9.5 Video Player Engine - Using ratio " 
				+ Float.toString(aspectRatio);
	}

	public void playAVI(String path) {
		System.out.println("Now Playing Video: " + path);
	}

}
