package com.ezekielbaniaga.springcoretest.beans;

public interface ConverterEngine {
	void convertFile(String path, String newPath);
}
