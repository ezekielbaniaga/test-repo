package com.ezekielbaniaga.springcoretest.beans;

public interface AudioPlayerEngine extends MediaEngine {

	public void playMP3(String path);
}
