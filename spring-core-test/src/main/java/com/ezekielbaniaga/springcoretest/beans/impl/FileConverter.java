package com.ezekielbaniaga.springcoretest.beans.impl;

import com.ezekielbaniaga.springcoretest.beans.ConverterEngine;

public class FileConverter implements ConverterEngine {

	public void convertFile(String path, String newPath) {
		System.out.println("Converting file..." + path + " to " + newPath);
	}

}
