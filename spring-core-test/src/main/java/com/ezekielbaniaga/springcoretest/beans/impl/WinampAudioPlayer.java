package com.ezekielbaniaga.springcoretest.beans.impl;

import com.ezekielbaniaga.springcoretest.beans.AudioPlayerEngine;

public class WinampAudioPlayer implements AudioPlayerEngine {

	private String settings;

	public WinampAudioPlayer(String settings) {
		this.settings = settings;
	}
	
	public String getEngineName() {
		return "WinAmp 10.0 Engine - " + settings;
	}

	public void playMP3(String path) {
		System.out.println("Listening to audio file:" + path);
	}

}
