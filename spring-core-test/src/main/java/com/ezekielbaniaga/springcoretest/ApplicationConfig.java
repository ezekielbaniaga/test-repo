package com.ezekielbaniaga.springcoretest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ezekielbaniaga.springcoretest.beans.AudioPlayerEngine;
import com.ezekielbaniaga.springcoretest.beans.ConverterEngine;
import com.ezekielbaniaga.springcoretest.beans.VideoPlayerEngine;
import com.ezekielbaniaga.springcoretest.beans.impl.FileConverter;
import com.ezekielbaniaga.springcoretest.beans.impl.WinampAudioPlayer;
import com.ezekielbaniaga.springcoretest.beans.impl.YouTubeVideoPlayer;

@Configuration
@ComponentScan(basePackages={
	"com.ezekielbaniaga.springcoretest.components"
})
public class ApplicationConfig {

	@Bean
	AudioPlayerEngine audioEngine() {
		return new WinampAudioPlayer("bass=80, treble=50, midrange=20");
	}
	
	@Bean
	VideoPlayerEngine videoEngine() {
		return new YouTubeVideoPlayer(1.5f);
	}
	
	@Bean
	ConverterEngine converterEngine() {
		return new FileConverter();
	}
}
