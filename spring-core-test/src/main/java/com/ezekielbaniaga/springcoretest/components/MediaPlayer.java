package com.ezekielbaniaga.springcoretest.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ezekielbaniaga.springcoretest.beans.AudioPlayerEngine;
import com.ezekielbaniaga.springcoretest.beans.ConverterEngine;
import com.ezekielbaniaga.springcoretest.beans.VideoPlayerEngine;

@Component
public class MediaPlayer {

	@Autowired
	AudioPlayerEngine audioEngine;
	
	@Autowired
	VideoPlayerEngine videoEngine;
	
	@Autowired
	ConverterEngine converterEngine;
	
	public void info() {
		System.out.println("Audio Player initiated using engine:" + audioEngine.getEngineName());
		System.out.println("Video Player initiated using engine:" + videoEngine.getEngineName());
	}

	public void playFile(String file) {
		converterEngine.convertFile(file, file.replaceAll(".avi$", ".mov"));

		if (isFileMP3(file)) {
			audioEngine.playMP3(file);
		} else if (isFileAVI(file)) {
			videoEngine.playAVI(file);
		}
	}
	
	private boolean isFileMP3(String file) {
		return file.endsWith(".mp3");
	}
	
	private boolean isFileAVI(String file) {
		return file.endsWith(".avi");
	}
}
