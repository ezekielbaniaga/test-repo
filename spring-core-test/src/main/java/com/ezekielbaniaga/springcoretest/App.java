package com.ezekielbaniaga.springcoretest;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.ezekielbaniaga.springcoretest.components.MediaPlayer;

/**
 * Spring Core Test
 */
public class App {
    public static void main( String[] args ) {
    	AnnotationConfigApplicationContext context =
    		new AnnotationConfigApplicationContext(ApplicationConfig.class);

    	MediaPlayer player = context.getBean(MediaPlayer.class);
    	player.info();

    	player.playFile("/home/zek/musics/test.mp3");
    	player.playFile("/home/zek/videos/test.avi");
    }
}
