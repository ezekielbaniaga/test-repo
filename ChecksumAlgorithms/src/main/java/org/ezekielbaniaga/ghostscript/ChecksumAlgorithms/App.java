package org.ezekielbaniaga.ghostscript.ChecksumAlgorithms;

import java.io.File;
import java.io.FileInputStream;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	File file = new File("D:/test images/salinas/multipage/SALINAS.pdf");

    	// Preload file in Kernel's disk cache for more accurate
    	// benchmarks
    	FileInputStream fis = new FileInputStream( file );
    	fis.read();
    	fis.close();

    	XXHash.printHash( file );
    	Adler32Test.printHash( file );
    }
}
