package com.ezekielbaniaga.services.javaservicesmain;

public interface Validator {

	public boolean isValid(String input);
}
