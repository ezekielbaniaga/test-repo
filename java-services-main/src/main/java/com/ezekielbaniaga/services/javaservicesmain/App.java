package com.ezekielbaniaga.services.javaservicesmain;

import java.util.ServiceLoader;


/**
 * Hello world!
 */
public class App {

	private static ServiceLoader<Validator> services = ServiceLoader.load(Validator.class);

    public static void main( String[] args ) {
    	
    	for (Validator v : services) {

    		System.out.println("is hello valid?" +  v.isValid("hello"));
    		System.out.println("is services valid?" +  v.isValid("services"));

    	}
    }
}
