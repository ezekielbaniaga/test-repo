package com.ezekielbaniaga.services.javaservicesplugin;

import com.ezekielbaniaga.services.javaservicesmain.Validator;

public class MyValidator implements Validator {

	public boolean isValid(String input) {
		return input.equals("services");
	}

}
