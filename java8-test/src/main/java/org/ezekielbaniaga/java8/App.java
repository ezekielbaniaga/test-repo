package org.ezekielbaniaga.java8;


/**
 * Hello world!
 *
 */
public class App {

	public static void main( String[] args ) throws Exception {
//		new LambdaTry().tryIt();
//		new AdvancedLambdaTry().tryIt();
		new StreamsTry().tryIt();
	}
}
