package org.ezekielbaniaga.java8;


import java.util.function.Supplier;

import org.ezekielbaniaga.java8.model.Person;


public class AdvancedLambdaTry implements Try {

	@Override
	public void tryIt() throws Exception {
		Displayer displayer = new Displayer();
		displayer.printOut( () -> {
			Person p = new Person();
			p.name = "Ezekiel";
			return p.name;
		} );
	}

	private static class Displayer {

		// Using built-in function interfaces
		public void printOut( Supplier<String> function ) {
			System.out.println( function.get() );
		}
	}
}
