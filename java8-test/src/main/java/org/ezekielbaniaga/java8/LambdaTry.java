package org.ezekielbaniaga.java8;


/**
 * Format of Lambda:
 * argument list arrow body
 * (int x, int y) -> {//code here}
 * 
 * @author Peakielsam
 *
 */
public class LambdaTry implements Try {

	public void tryIt() throws Exception {
		Printer p = new Printer();

		p.print( "canon", () -> System.out.println( "Done" ) ); // body
		p.print( "epson", this::done2 ); // method reference
		p.print( "hp", Dummy::new ); // constructor reference

		System.out.println( "Printing test page..." );
		p.test( ( m ) -> System.out.println( m ) );

		System.out.println( "Printing test page again..." );
		p.test( System.out::println );

	}


	public void done2() {
		System.out.println( "Done 2" );
	}


	private static class Printer {

		public void print( String deviceName, DoneFunction function ) {
			System.out.println( "Printing on device:" + deviceName );
			function.done();
		}


		public void test( QuickTestFunction function ) {
			for ( int i = 1; i < 11; i++ ) {
				function.quickTest( "Testing " + i );
			}
		}
	}

	private static interface DoneFunction {

		public void done();
	}

	private static interface QuickTestFunction {

		public void quickTest( String message );
	}

	private static class Dummy {

		public Dummy() {
			System.out.println( "Hi I'm Dummy" );
		}
	}
}
