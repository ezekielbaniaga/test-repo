package org.ezekielbaniaga.java8;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.ezekielbaniaga.java8.model.Person;


public class StreamsTry implements Try {

	@Override
	public void tryIt() throws Exception {
		List<Person> persons = createSamplePersons();

		/*
		 * Filter and For Each Demo
		 * And here's without streams:
		 * for (Person p : persons) {
		 *     if (p.age > 20) {
		 *         System.out.println( p.name );
		 *     }
		 * }
		 */

		persons.stream().filter( p -> p.age >= 20 ).forEach( p -> System.out.println( p.name ) );
		
		
		// Reusable
		Predicate<Person> filterCondition = p -> p.age >= 20;
		persons.stream().filter( filterCondition ).forEach( this::iterateFunction );
		
		
		// Benchmark streaming
		persons = createManySamplePersons();
		
		// Traditional
		long startTime = System.nanoTime();
		long endTime;

		List<Person> allTwentys = new ArrayList<>();
		StringBuilder concat = new StringBuilder();
        for (Person p : persons) {
            if (p.age >= 20) {
            	allTwentys.add( p );
            }
		}
        
        for (Person p : allTwentys) {
        	concat.append( p.name );
        }

        endTime = System.nanoTime();
        System.out.println( "Traditional filter ended in " + ((endTime - startTime) / 1000000)  + " ms");
        
        // Stream
        startTime = System.nanoTime();
        StringBuilder concat2 = new StringBuilder();
        persons.stream().filter( filterCondition ).forEach( p -> concat2.append( p.name ) );
        endTime = System.nanoTime();
        System.out.println( "Stream filter ended in " + ((endTime - startTime) / 1000000)  + " ms");

        // Built-in concat by Collectors
        startTime = System.nanoTime();
        persons.stream().filter( filterCondition ).map( p -> p.name ).collect( Collectors.joining() );
        endTime = System.nanoTime();
        System.out.println( "Stream filter using predefined Collectors.joining() ended in " + ((endTime - startTime) / 1000000)  + " ms");
        

        // Most of the time we do this boolean name 'hasXXX'. It is easy now
        boolean hasAgeOf21 = persons.stream().anyMatch( p -> p.age == 21 );
        System.out.println( "has match?" + hasAgeOf21 );
        
        // Who's older?
        Optional<Person> p = persons.stream().max( (p1, p2) -> p1.age-p2.age);
        System.out.println( "The oldest:" + p.get().name );
        

	}
	
	private void iterateFunction(Person p) {
		System.out.println( p.name );
	}


	private List<Person> createSamplePersons() {
		List<Person> persons = new ArrayList<>();

		Person p = new Person();
		p.name = "John";
		p.age = 21;
		persons.add( p );

		p = new Person();
		p.name = "Jenny";
		p.age = 19;
		persons.add( p );

		p = new Person();
		p.name = "Leah";
		p.age = 18;
		persons.add( p );

		p = new Person();
		p.name = "David";
		p.age = 25;
		persons.add( p );


		return persons;
	}

    private List<Person> createManySamplePersons() {
		List<Person> persons = new ArrayList<>();

		// million records
		for (int i=1; i<1000001; i++) {
            Person p = new Person();
            p.name = "John";
            p.age = 21;
            persons.add( p );

            p = new Person();
            p.name = "Jenny";
            p.age = 19;
            persons.add( p );

            p = new Person();
            p.name = "Leah";
            p.age = 18;
            persons.add( p );

            p = new Person();
            p.name = "David";
            p.age = 25;
            persons.add( p );
		}


		return persons;
	}
}
