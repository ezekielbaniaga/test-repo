package com.ezekielbaniaga.xorencryption;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final int KEY = 345;

    public static void main( String[] args ) {
    	long startTime = System.currentTimeMillis();
    	File src = new File("D:\\encrypted");

    	for (File f : src.listFiles()) {
    		System.out.println("xoring..."+f.getAbsolutePath());
    		xor(f);
    	}
    	
    	System.out.println("Done in " + (System.currentTimeMillis() - startTime) + "ms");
    }
    
    public static void xor(File f) {
    	try {
    		byte[] data = new byte[1024];

			RandomAccessFile rac = new RandomAccessFile(f, "rw");
			int len = rac.read(data);
			
			byte[] newData = new byte[len];
			
			for (int i=0; i<data.length; i++) {
				newData[i] = (byte)(0xFF & ((int)data[i] ^ KEY));
			}
			
			rac.seek(0);
			rac.write(newData);
			
			rac.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
