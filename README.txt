Test Repository

Lots of sample codes to easily share 
techniques and algorithms in 
programming.

The following entries are placed in .gitignore.
They are IDE specific files.
/.settings
/.project
/.classpath

Running Web Apps
mvn clean install
mvn tomcat:run - for Running using tomcat 6
mvn tomcat7:run - for tomcat 7

Testing Jenkins 3
