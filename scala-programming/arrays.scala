val greet = new Array[String](3)

// arrays are accessed by parenthesis
// because Array defines 'apply' method
// In Scala, when you apply parenthesis
// to a variable, you are then invoking
// the method 'apply' only if it is
// defined in the Object.
// If assignment has been made with the
// variable that uses parenthesis,
// it will call 'update' method. e.g.
// person(i) = 'abc' is equal to
// person.update(i, 'abc')

greet(0) = "Hello"
greet(1) = ", "
greet(2) = "World"

for (i <- 0 to 2)
    print(greet(i))



// playing with (Int).to(Int)
val ints = 0 to 10
ints.foreach(println)


// another way to initialize Array. 
// below calls 'apply' that takes 
// variable number of arguments
// Same as Array.apply("zero", "one", "two")
val numNames = Array("zero", "one", "two")


// Lists are immutable unlike in Java
// thus you can't change the value
// of an element
// ::: is a function that concatenates Lists

val oneTwo = List(1,2)
val threeFour = List(3,4)
val oneTwoThreeFour = oneTwo ::: threeFour

println("" + oneTwo + " and " + threeFour + " were not mutated.")
println("Thus, " + oneTwoThreeFour + " is a new list.")

// :: is a function that prepends a new element to 
// the beginning of an existing list and returning
// the resulting list
// Note: expression 1::twoThree has a method called '::'
// This ends in a colon thus it is right operand. Meaning
// This it can be rewritten as twoThree.::(1)
val twoThree = List(2,3)
val oneTwoThree = 1 :: twoThree
println(oneTwoThree)


// Nil is a shorthand to an empty List
val oneTwoThreeNil = 1 :: 2 :: 3 :: Nil
println("One Two Three Nil " + oneTwoThreeNil)


