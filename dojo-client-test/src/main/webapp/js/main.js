require([
    'dojo/dom',
    'dojo/request',
	'dojo/domReady!'
], function(dom, request) {
	
	var content = dom.byId('content');
	
	//content.innerHTML = "Hello World";
	
	request('http://localhost:8080/spring-rest-test/api/persons/forweb').then(
		function(text) {
			content.innerHTML = text;
		}
    );
});