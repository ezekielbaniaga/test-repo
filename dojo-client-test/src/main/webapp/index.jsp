<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Dojo Test Client</title>
</head>
<body>
    <h1 id="greeting">Test Client (HTML + DOJO Toolkit)</h1>
    <div id="content"></div>
	
    <script src="//ajax.googleapis.com/ajax/libs/dojo/1.9.2/dojo/dojo.js" data-dojo-config="async: true"></script>
    <script src="js/main.js" type="text/javascript"></script>
</body>
</html>