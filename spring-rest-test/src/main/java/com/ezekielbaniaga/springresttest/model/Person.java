package com.ezekielbaniaga.springresttest.model;

public class Person {
	public String lastName;
	public String firstName;
	public String middleName;
	public int age;
}
