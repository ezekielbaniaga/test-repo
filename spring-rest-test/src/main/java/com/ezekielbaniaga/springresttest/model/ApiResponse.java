package com.ezekielbaniaga.springresttest.model;

public class ApiResponse {
	public boolean success;
	public String message;
	public Object data;
	
	public static ApiResponse createSuccessResponse(Object data) {
		ApiResponse response = new ApiResponse();
		response.success = true;
		response.data = data;

		return response;
	}
}
