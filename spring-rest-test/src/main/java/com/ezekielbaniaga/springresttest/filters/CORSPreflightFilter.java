package com.ezekielbaniaga.springresttest.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

/**
 * As specified on http://en.wikipedia.org/wiki/Cross-origin_resource_sharing,
 * there's a preflight request that mandates the browsers to send HTTP OPTIONS
 * request header before going on to the actual request.
 * 
 * Other sources: https://developer.mozilla.org/en/docs/HTTP/Access_control_CORS
 * 
 * TODO: Not working
 * Filter disabled at web.xml
 * 
 * @author Ezekiel
 */
public class CORSPreflightFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, 
		FilterChain filterChain) throws ServletException, IOException {

		if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
			response.addHeader("Access-Control-Allow-Origin", "*");
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
			response.addHeader("Access-Control-Allow-Headers", "Content-Type, x-http-method-override");
			response.addHeader("Access-Control-Max-Age", "1800"); // 30mins then ask for preflight again.
		}
		
		filterChain.doFilter(request, response);
	}

}
