package com.ezekielbaniaga.springresttest.filters;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.io.IOUtils;

/**
 * Handles decryption and encryption and of request and response.
 * Using AES algorithm.
 * 
 * @author Peakielsam
 */
public class AESFilter implements Filter {

	private static final byte[] KEY = new byte[]{'T','h','i','s','-','s','e','c','r','e','t','-','k','e','y','?'};
	private Cipher encCipher, decCipher;
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletResponse hresp = (HttpServletResponse) resp;
		HttpServletRequest  hreq  = (HttpServletRequest) req;
		
		AESServletResponseWrapper wresp = new AESServletResponseWrapper(hresp);
		AESServletRequestWrapper  wreq  = new AESServletRequestWrapper(hreq);
		
		chain.doFilter(wreq, wresp);
		
		byte[] data = encryptAES(wresp.getResponseData());

		OutputStream os = hresp.getOutputStream();
		os.write(data);
		os.flush();
		os.close();
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
        try {
            encCipher = Cipher.getInstance("AES");
            encCipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(KEY, "AES"));
            
            decCipher = Cipher.getInstance("AES");
			decCipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(KEY, "AES"));
        } catch (Exception ex) {
        	ex.printStackTrace();
        }	
    }

    public byte[] decryptAES(byte[] subject) {
		try {
			byte[] dec = Base64.getDecoder().decode(subject);
			return decCipher.doFinal(dec);
		} catch (Exception ex) {
        	ex.printStackTrace();
        }
		
		return null;
	}

    public byte[] encryptAES(byte[] subject) {
        try {
        	byte[] enc = encCipher.doFinal(subject);
        	return Base64.getEncoder().encode(enc);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }

        return null;
    }
		
    @Override
	public void destroy() {
	}
    
    private class AESServletRequestWrapper extends HttpServletRequestWrapper {

    	private HttpServletRequest request;
    	private byte[] contentData;
    	
    	public AESServletRequestWrapper(HttpServletRequest request) {
			super(request);
			this.request = request;
		}
    	
    	private void parseContentData() {
    		try {
    			
    			contentData = IOUtils.toByteArray( request.getInputStream() );
				
				System.out.println("Content Data from Request:");
				System.out.println(new String(contentData));
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    	

		@Override
    	public ServletInputStream getInputStream() throws IOException {
			parseContentData();
			return new DecrytedServletInputStreamImpl(this.contentData);
    	}
		
		@Override
		public BufferedReader getReader() throws IOException {
			parseContentData();
			return new BufferedReader(new InputStreamReader(new ByteArrayInputStream(contentData),"UTF-8"));
		}
    }
    
    private class DecrytedServletInputStreamImpl extends ServletInputStream {
    	byte[] data;
    	int idx = 0;

    	public DecrytedServletInputStreamImpl(byte[] data) {
    		byte[] decData = decryptAES(data);
    		this.data = decData;
		}
    	
    	@Override
        public int read() throws IOException {
    		if (idx == data.length)
    			return -1;
    		
            return data[idx++];
        }
    }
    
    private static class AESServletResponseWrapper extends HttpServletResponseWrapper {

    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	private PrintWriter writer = new PrintWriter(baos);

		public AESServletResponseWrapper(HttpServletResponse response) {
			super(response);
		}
		
		@Override
		public ServletOutputStream getOutputStream() throws IOException {
			return new ServletOutputStream() {
				
				@Override
				public void write(int b) throws IOException {
					baos.write(b);
				}
				
				@Override
				public void write(byte[] b) throws IOException {
					baos.write(b);
				}
			};
		}
		
		@Override
		public PrintWriter getWriter() throws IOException {
			return writer;
		}
    	
		@Override
		public void flushBuffer() throws IOException {
			if (writer != null) {
				writer.flush();
			} else if (baos != null) {
                baos.flush();
			}
		}

		public byte[] getResponseData() {
			return baos.toByteArray();
		}
    }
}
