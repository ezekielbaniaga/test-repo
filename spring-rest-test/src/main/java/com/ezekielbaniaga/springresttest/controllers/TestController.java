package com.ezekielbaniaga.springresttest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/test")
public class TestController {

	@RequestMapping(value="/welcome", method=RequestMethod.GET)
	public String welcome(ModelMap map) {
		
		map.addAttribute("message","Maven Proj");
		
		return "index";
	}
}
