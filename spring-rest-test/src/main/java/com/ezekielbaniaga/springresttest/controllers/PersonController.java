package com.ezekielbaniaga.springresttest.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ezekielbaniaga.springresttest.model.ApiResponse;
import com.ezekielbaniaga.springresttest.model.Person;

@Controller
@RequestMapping("/persons")
public class PersonController {

	@ResponseBody
	@RequestMapping(method=RequestMethod.GET)
	public ApiResponse findAllPersons() {
		List<Person> persons = new ArrayList<Person>();
		
		Person p = new Person();
		p.firstName = "Juan";
		p.lastName = "Tamad";
		p.middleName = "L.";
		p.age = 20;

		persons.add(p);
		
		p = new Person();
		p.firstName = "Jose";
		p.lastName = "Protacio";
		p.age = 78;
		
		persons.add(p);
		
		return ApiResponse.createSuccessResponse(persons);
	}
	
	@ResponseBody
	@RequestMapping(value="/forweb", method=RequestMethod.GET)
	public ResponseEntity<ApiResponse> findAllPersonsForWeb() {
		List<Person> persons = new ArrayList<Person>();
		
		Person p = new Person();
		p.firstName = "Juan";
		p.lastName = "Tamad";
		p.middleName = "L.";
		p.age = 20;

		persons.add(p);
		
		p = new Person();
		p.firstName = "Jose";
		p.lastName = "Protacio";
		p.age = 78;
		
		persons.add(p);
		
		ApiResponse response = ApiResponse.createSuccessResponse(persons);
		
		HttpHeaders responseHeader = new HttpHeaders();
		responseHeader.add("Access-Control-Allow-Origin", "*");
		
		return new ResponseEntity<ApiResponse>(response, responseHeader, HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public ApiResponse updatePerson(@RequestBody Person p) {
		ApiResponse response = ApiResponse.createSuccessResponse("Successfully Updated");
		
		System.out.println("-----------------RECEIVED UPDATE----------------------");
		System.out.println(p.lastName + ", " + p.firstName);
		
		return response;
	}
}
