package com.ezekielbaniaga.springresttest;

import org.springframework.context.annotation.Configuration;

/**
 * Declare Application Context here. e.g. beans and controllers
 * 
 * @author Ezekiel
 */
@Configuration
public class AppConfig {

}
