package com.ezekielbaniaga.springaoptest.components;

import org.springframework.stereotype.Component;

@Component
public class MyDBSystem {

	public void startSystem() {
		System.out.println("Starting database system");
	}
	
	public void addNewDatabase() {
		System.out.println("Adding new database system.");
	}
	
	public void proceedOrNot(Boolean proceed) {
		System.out.println("I'm proceeding...");
	}
}
