package com.ezekielbaniaga.springaoptest;

import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.aop.support.RegexpMethodPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ezekielbaniaga.springaoptest.aop.MyBeforeAfterAdvice;
import com.ezekielbaniaga.springaoptest.aop.MyMethodInterceptor;
import com.ezekielbaniaga.springaoptest.aop.MyThrowsAdvice;
import com.ezekielbaniaga.springaoptest.beans.MyDatabase;

@Configuration
@ComponentScan(basePackages={
	"com.ezekielbaniaga.springaoptest.components"
})
public class AppConfig {

	@Bean 
	MyDatabase db() {
		MyDatabase db = new MyDatabase();
		
		ProxyFactoryBean proxy = new ProxyFactoryBean();
		proxy.setTarget(db);
		proxy.addAdvisor(advisor());
		proxy.addAdvisor(throwableAdvisor());
		
		return (MyDatabase) proxy.getObject();
	}
	
	@Bean
	MyBeforeAfterAdvice advice() {
		return new MyBeforeAfterAdvice();
	}
	
	@Bean
	MyThrowsAdvice throwsAdvice() {
		return new MyThrowsAdvice();
	}
	
	@Bean
	MyMethodInterceptor interceptor() {
		return new MyMethodInterceptor();
	}
	
	@Bean 
	Advisor interAdvisor() {
		RegexpMethodPointcutAdvisor advisor = new RegexpMethodPointcutAdvisor();
		advisor.setAdvice(interceptor());
		advisor.setPatterns(new String[]{".*proceedOrNot.*"}); 
		
		return advisor;	
	}
	
	@Bean
	Advisor advisor() {
		RegexpMethodPointcutAdvisor advisor = new RegexpMethodPointcutAdvisor();
		advisor.setAdvice(advice());
		advisor.setPatterns(new String[]{".*add.*", ".*find.*"}); // apply AOP only on methods with names addXXX and findXXX
		
		return advisor;
	}

	@Bean
	Advisor throwableAdvisor() {
		RegexpMethodPointcutAdvisor advisor = new RegexpMethodPointcutAdvisor();
		advisor.setAdvice(throwsAdvice());
		advisor.setPatterns(new String[]{".*add.*", ".*find.*"}); // apply AOP only on methods with names addXXX and findXXX
		
		return advisor;
	}
	
	// start configuring auto proxy
	
	@Bean
	BeanNameAutoProxyCreator autoProxy() {
		BeanNameAutoProxyCreator autoProxier = new BeanNameAutoProxyCreator();
		autoProxier.setBeanNames(new String[]{"*System"});
		autoProxier.setInterceptorNames(new String[]{"advisor", "interAdvisor"});
		
		return autoProxier;
	}
	
}
