package com.ezekielbaniaga.springaoptest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.ezekielbaniaga.springaoptest.beans.MyDatabase;
import com.ezekielbaniaga.springaoptest.components.MyDBSystem;

/**
 * Hello world!
 *
 */
public class App { 
	
	public static void main( String[] args ) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
		
//		MyDatabase db = ctx.getBean(MyDatabase.class);
		
//		db.addToDB("zek");

//		String found = db.findFromDB();
//		System.out.println("Found in db:" + found);
		
		// Uncomment this to test how Spring catches exceptions.
		//db.findAnother();
		
//		db.actionPack();
		
		// Now this component is auto proxied, to prevent rewriting lots of proxy
		MyDBSystem system = ctx.getBean(MyDBSystem.class);
//		system.startSystem();
//		system.addNewDatabase();
		
		system.proceedOrNot(true); // useful for validations
    }
}
