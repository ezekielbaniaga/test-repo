package com.ezekielbaniaga.springaoptest.aop;

import org.springframework.aop.ThrowsAdvice;

public class MyThrowsAdvice implements ThrowsAdvice {

	public void afterThrowing(Throwable throwable) throws Exception {
		System.out.println("Error encountered and catched by Spring AOP!");
		RuntimeException rex = new RuntimeException("Encountered an error during db operation");
		rex.initCause(throwable);
		
		throw rex;
	}
}
