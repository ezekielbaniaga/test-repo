package com.ezekielbaniaga.springaoptest.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;


public class MyMethodInterceptor implements MethodInterceptor {

	public Object invoke(MethodInvocation invocation) throws Throwable {
		
		if (invocation.getArguments()[0] == Boolean.TRUE) {
			invocation.proceed();
		} else {
			System.out.println("won't proceed");
		}
		
		return null;
	}

	
}
