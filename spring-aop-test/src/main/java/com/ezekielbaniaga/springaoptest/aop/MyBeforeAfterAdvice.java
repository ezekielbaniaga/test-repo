package com.ezekielbaniaga.springaoptest.aop;

import java.lang.reflect.Method;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;


public class MyBeforeAfterAdvice implements MethodBeforeAdvice, AfterReturningAdvice {

	public void before(Method method, Object[] args, Object target)
			throws Throwable {
		
		System.out.println("Spring AOP Before");
	}

	public void afterReturning(Object returnValue, Method method,
			Object[] args, Object target) throws Throwable {

		System.out.println("Spring AOP After:" + returnValue);
	}
}
