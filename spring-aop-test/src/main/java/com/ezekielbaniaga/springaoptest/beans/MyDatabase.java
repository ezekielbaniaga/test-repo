package com.ezekielbaniaga.springaoptest.beans;

public class MyDatabase {

	public void addToDB(String val) {
		System.out.println("Added to database:" + val);
	}
	
	public String findFromDB() {
		return "AOP";
	}
	
	public String findAnother() {
		int i = 1/0;
		return null;
	}
	
	public void actionPack() {
		System.out.println("This will get invoked but no AOP handles it.");
	}
}
