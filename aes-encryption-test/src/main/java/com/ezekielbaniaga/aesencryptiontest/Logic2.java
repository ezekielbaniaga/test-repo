package com.ezekielbaniaga.aesencryptiontest;

import org.apache.commons.codec.binary.Base64;

public class Logic2 {

	public static String encrypt(String data) throws Exception {
		byte[] encVal = App.encryptCipher2.doFinal(data.getBytes("UTF-8"));
		String encryptedValue = Base64.encodeBase64String(encVal);
		return encryptedValue;
	}
	
	public static String decrypt(String encryptedData) throws Exception {
		byte[] decodedValue = Base64.decodeBase64(encryptedData);
		byte[] decValue = App.decryptCipher2.doFinal(decodedValue);
		String decryptedValue = new String(decValue,"UTF-8");
		return decryptedValue;
	}
}
