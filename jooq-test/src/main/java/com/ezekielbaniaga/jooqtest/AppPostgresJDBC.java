package com.ezekielbaniaga.jooqtest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AppPostgresJDBC {

	public static void main(String[] args) {
		Connection conn = null;
		
        String userName = "postgres";
        String password = "m@s@nt1ng";
        String url = "jdbc:postgresql://192.168.0.136:5432/users";

		try {
            conn = DriverManager.getConnection(url, userName, password);
            
			long startTime = System.nanoTime();

			// 10K Row test
//            PreparedStatement ps = conn.prepareStatement("INSERT INTO users(username, name) VALUES(?,?)");
//			for (int i=1; i<=10000; i++) { // first trial record: Done in 11896 ms.
//	            ps.setString(1, "mytest"+"_"+i);
//	            ps.setString(2, "My Test");
//	            ps.execute();
//			}
			
			// 10K Row test 500 batch
//			PreparedStatement ps = conn.prepareStatement("INSERT INTO users(username, name) VALUES(?,?)");
//			for (int i=1; i<=10000; i++) { // first trial record: Done in 604ms, 802ms, 617ms, 549ms(no sysout,hehe)
//	            ps.setString(1, "mytest"+"_"+i);
//	            ps.setString(2, "My Test");
//	            ps.addBatch();
//	            
//	            if (i%500 == 0) {
//					System.out.println("Executing 500 batch"); 
//		            ps.executeBatch();
//		            ps.clearBatch();
//	            }
//			}
			
			// Read 10K Rows
			Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * from users");
			
			int cnt=0;
            while (rs.next()) {
//	            System.out.println(rs.getString("username")); 
				cnt++;
            }
			System.out.println("COUNT:" + cnt); // first trial record: Done in 77 ms.

			// Read one row
//			Statement st = conn.createStatement();
//            ResultSet rs = st.executeQuery("SELECT name from users WHERE username = 'mytest_9000'");
//            rs.next();
//            System.out.println(rs.getString(1)); //first trial record: Done in 30 ms. 27 ms, 27 ms, 28 ms, 27 ms
            
			System.out.printf("Done in %d ms.\n", (System.nanoTime() - startTime) / 1000000);
            
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			// close connection
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
