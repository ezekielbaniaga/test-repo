package com.ezekielbaniaga.jooqtest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.jooq.DSLContext;
import org.jooq.Delete;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.Update;
import org.jooq.impl.DSL;

import com.ezekielbaniaga.jooqtest.jooq.Sequences;
import com.ezekielbaniaga.jooqtest.jooq.tables.Testtable;
import com.ezekielbaniaga.jooqtest.jooq.tables.records.TesttableRecord;

import static com.ezekielbaniaga.jooqtest.jooq.tables.Testtable.TESTTABLE;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
    	
    	Connection conn = null;

        String userName = "postgres";
        String password = "m@s@nt1ng";
        String url = "jdbc:postgresql://192.168.0.204:5432/zekdb";

        try {
        	// Plain JDBC
            conn = DriverManager.getConnection(url, userName, password);
            System.out.println("Is connected? " + !conn.isClosed());
            
            // jOOQ usage starts here
            DSLContext context = DSL.using(conn, SQLDialect.POSTGRES);
            
            // Fetch records
            Result<Record> result = context.select().from(TESTTABLE).fetch();
            
            for (Record r : result) {
            	long id = r.getValue(TESTTABLE.TBID);
            	String data = r.getValue(TESTTABLE.TESTDATA);
            	
            	System.out.printf("%d\t%s\n", id, data);
            }
            
            System.out.println("\n\nJSON:" + result.formatJSON());
            System.out.println("XML:" + result.formatXML());
            System.out.println("CSV:" + result.formatCSV());
            System.out.println("HTML:" + result.formatHTML());
            
            /*
             * Insert Record
            context.insertInto(TESTTABLE, TESTTABLE.TBID, TESTTABLE.TESTDATA)
            	  .values(100, "asdf|asdf").execute();
	    	 */
            
            /*
             * Update Record with sql code generator
            Update updateQuery = context.update(TESTTABLE).set(TESTTABLE.TESTDATA, "helloasdf")
                   .where(TESTTABLE.TBID.equal(100));
            System.out.println("SQL Statement:" + updateQuery.getSQL());
            updateQuery.execute();
            */
            
            /*
             * Delete Record
            context.delete(TESTTABLE).where(TESTTABLE.TBID.equal(100)).execute();
             */
            
            /**
             * Fetch all using auto-generated table records
             */

            List<TesttableRecord> ttr = context.selectFrom(TESTTABLE).orderBy(TESTTABLE.TBID).fetchInto(TesttableRecord.class);
            
            for (TesttableRecord tr : ttr) {
            	System.out.println("Test table record...");
            	System.out.println(tr.getTbid());
            	System.out.println(tr.getTestdata());
            }
            
            /**
             * Update using auto-generated table records
             */
            TesttableRecord tr = ttr.get(0);
            tr.refresh();
            tr.setTestdata("auto gen - " + UUID.randomUUID().toString().substring(0, 10));
            tr.store();

            /**
             * Delete last record using table record
             */
            tr = ttr.get(ttr.size()-1);
            tr.refresh();
            tr.delete();
            		
            /**
             * Inserting record through TesttableRecord
             */
            TesttableRecord trNew = new TesttableRecord();
            trNew.attach(context.configuration());
            trNew.setTbid(context.nextval(Sequences.DEFAULT_SEQ)); // <---- specifying sequence though PostgreSQL can handle default values.
            trNew.setTestdata("new rec - " + UUID.randomUUID().toString().substring(0, 10));
            trNew.store();

        } catch (Exception e) {
            // For the sake of this tutorial, let's keep exception handling simple
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ignore) {
                }
            }
        }
    }
}
