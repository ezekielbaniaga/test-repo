package org.ezekielbaniaga.ghostscript;

import org.ghost4j.Ghostscript;
import org.ghost4j.GhostscriptException;

/**
 * Hello world!
 *
 */
public class App {

	public static void main( String[] args ) {
		long startTime = System.nanoTime();
		long elapsedTime;

		convertToPNG();
		
		elapsedTime = System.nanoTime() - startTime;
		
		System.out.printf("%d ns (%d ms)", elapsedTime, elapsedTime/1000000);
	}
	
	public static void convertToPNG() {
		
		Ghostscript gs = Ghostscript.getInstance();

		// http://ghostscript.com/doc/9.18/Use.htm
		String[] gsArgs = {
			"-dNumRenderingThreads=8",
			"-dNOPAUSE",
//			"-dNODISPLAY",
			"-dBATCH",
			"-dBandHeight=100",
			"-dBandBufferSpace=500000000",
			"-dBufferSpace=1000000000",
			"-sDEVICE=png16m", 
//			"-dJPEGQ=100",
			"-sBandListStorage=memory",
			"-sOutputFile=D:\\test images\\salinas\\multipage\\image%d.png",
			"-r150",
			"-q",
			"D:\\test images\\salinas\\multipage\\SALINAS.pdf",
		};

		try {
			gs.initialize( gsArgs );
			gs.exit();
		} catch ( GhostscriptException e ) {
			e.printStackTrace();
		}
		
	}
}
