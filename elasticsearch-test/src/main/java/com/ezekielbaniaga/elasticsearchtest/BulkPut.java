package com.ezekielbaniaga.elasticsearchtest;

import java.io.IOException;

import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;


// for jsonBuilder
import static org.elasticsearch.common.xcontent.XContentFactory.*;

/**
 * Erroneous 
 * 
 * @author Ezekiel
 */
public class BulkPut {

	public static void test() throws IOException {
		Client client = new TransportClient()
	    	.addTransportAddress(new InetSocketTransportAddress("192.168.0.166", 9300));
//	    	 other host -> .addTransportAddress(new InetSocketTransportAddress("192.168.0.247", 9301));
		
		BulkProcessor bulkProcessor = BulkProcessor.builder(client, new BulkProcessor.Listener() {
			
			public void beforeBulk(long arg0, BulkRequest arg1) {
				System.out.println("Before Bulk");
			}
			
			public void afterBulk(long arg0, BulkRequest arg1, Throwable arg2) {
				System.out.println("After Bulk with Error:");
				arg2.printStackTrace();
			}
			
			public void afterBulk(long arg0, BulkRequest arg1, BulkResponse arg2) {
				System.out.println("After Bulk: " + arg0);
			}
		})
		.setBulkActions(100000)
		.setBulkSize(new ByteSizeValue(900, ByteSizeUnit.MB))
		.setConcurrentRequests(1)
		.build();
		
		// 17k records, 200actions, 500mb, 0 req, 4 ES nodes. = 14sec (14249ms)
		// 17k records, 200actions, 500mb, 1 req, 4 ES nodes. = 8sec (8842ms)
		// 17k records, 200actions, 500mb, 1 req, 8 ES nodes. = 11sec (10947ms)
		// 1.7m records, 200actions, 500mb, 1 req, 4 ES nodes. = 361sec (360729ms) (6mins)
		// 170k records, 20,000 actions, 900mb, 1 req, 4 ES nodes. 11sec (10737ms)
		
		long startTime = System.nanoTime();
		for (int id=1; id<=170000; id++) {
			bulkProcessor.add(
				new IndexRequest("persons", "student", String.valueOf(id))
				.source(
					jsonBuilder().startObject()
						.field("id", String.valueOf(id))
						.field("prefix", "E")
						.field("name", "Ezekiel")
						.field("age", "20")
						.endObject().bytes().toBytes() //it's important to use toBytes() instead of array()
				)
			);
		}
		
		bulkProcessor.flush();
		bulkProcessor.close();
		
		System.out.println("end in " + ((System.nanoTime()-startTime)/1000000) + "ms");

		client.close();
	}
}
