package com.ezekielbaniaga.elasticsearchtest;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

public class SimpleBulkPut {

	
	public static void test() throws Exception {
		Client client = new TransportClient()
	    	.addTransportAddress(new InetSocketTransportAddress("192.168.0.166", 9300));

		BulkRequestBuilder bulk = initBulk(client);
		
		int bulk_size = 100000; // hundred thousand
		
		// 1.7 million done in 171265ms (2.8mins) 
		long startTime = System.nanoTime();
		for (int id=1; id<=1700000; id++) {
			bulk.add(
				client.prepareIndex("persons", "student", String.valueOf(id))
					.setSource(
						jsonBuilder().startObject()
							.field("id", String.valueOf(id))
							.field("prefix", "E")
							.field("name", "Ezekiel")
							.field("age", "20")
						.endObject().bytes().toBytes() //it's important to use toBytes() instead of array()
			));
			
			if (bulk.numberOfActions() == bulk_size) {
				commitBulk(bulk);
				bulk = initBulk(client);
			}
		}
		
		// Process Remaining
		if (bulk.numberOfActions() > 0) {
			commitBulk(bulk);
		}
		
		System.out.println("end in " + ((System.nanoTime()-startTime)/1000000) + "ms");

		client.close();
	}
	
	private static void commitBulk(BulkRequestBuilder bulk) {
		BulkResponse bulkResponse = bulk.execute().actionGet();
		if (bulkResponse.hasFailures()) {
			System.out.println("has errors");
		} else {
			System.out.println("Bulk done in " + bulkResponse.getTookInMillis() + "ms");
		}
	}
	
	private static BulkRequestBuilder initBulk(Client client) {
		BulkRequestBuilder bulk = client.prepareBulk();
		bulk.setRefresh(false);
		return bulk;
	}
}
