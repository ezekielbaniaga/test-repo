package com.ezekielbaniaga.elasticsearchtest;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

public class SampleGet {

	public static void test() {
		Client client = new TransportClient()
	    	.addTransportAddress(new InetSocketTransportAddress("192.168.0.247", 9300));
	    	// other host -> .addTransportAddress(new InetSocketTransportAddress("192.168.0.247", 9301));

    	long startTime = System.nanoTime();
    	StringBuilder builder = new StringBuilder();
    	for (int i=1; i<=100; i++) {
	    	GetResponse resp = client.prepareGet("persons", "student", String.valueOf(i))
	    			.execute().actionGet();
	    	
	    	builder.append(resp.getSourceAsString()).append("\n");
    	}
    	
    	System.out.println(builder.toString());
    	System.out.println("Ended in " + (System.nanoTime() - startTime));
    	
    	client.close();
    	
	}
}
