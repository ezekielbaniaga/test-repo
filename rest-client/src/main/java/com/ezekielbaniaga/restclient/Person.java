package com.ezekielbaniaga.restclient;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class Person {
	public String lastName;
	public String firstName;
	public String middleName;
	public int age;
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
