package com.ezekielbaniaga.restclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JavaNetURLTester {
	
	private String urlStr;
	
	public JavaNetURLTester(String url) {
		this.urlStr = url;
	}

	public String getData() throws Exception {
		URL url = new URL(urlStr);
    	HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    	
    	conn.setUseCaches(false);
    	conn.setDoInput(true);
    	conn.setRequestMethod("GET");
    	conn.connect();

    	int responseCode = conn.getResponseCode();
    	
    	if (responseCode == 200) { // HTTP 200-OK
    		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		
    		StringBuilder buffer = new StringBuilder();
    		String line = null;
    		
    		while ((line = reader.readLine()) != null) {
    			buffer.append(line);
    		}

    		reader.close();
    		
    		
    		return buffer.toString();
    	}

    	conn.disconnect();
    	
    	return null;
	}
	
	
	public void postData(String data) throws Exception {
		System.out.println("Sending..." + data);
		URL url = new URL(urlStr);
    	HttpURLConnection conn = (HttpURLConnection) url.openConnection();

    	conn.setRequestProperty("Accept", "*/*");
    	conn.setRequestProperty("Accept-Charset", "UTF-8");
    	conn.setRequestProperty("Content-Type", "application/json");
		conn.setUseCaches(false);
    	conn.setDoInput(true);
    	conn.setDoOutput(true);
    	conn.connect();
    	
    	conn.getOutputStream().write(data.getBytes());
    	
    	System.out.println(conn.getResponseCode());
    	System.out.println(conn.getResponseMessage());
    	conn.disconnect();

	}
	
}
