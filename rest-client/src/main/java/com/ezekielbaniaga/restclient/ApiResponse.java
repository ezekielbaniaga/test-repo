package com.ezekielbaniaga.restclient;

public class ApiResponse<T> {
	public boolean success;
	public String message;
	public T data;
}
