package com.ezekielbaniaga.restclient;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

/**
 * REST Clients
 */
public class App {

    public static void main( String[] args ) throws Exception {
    	
    	/** Uncomment to test getting data from REST Service */
    	JavaNetURLTester tester = new JavaNetURLTester("http://localhost:7083/spring-rest-test/api/persons");
    	//ApacheHTTPComponentsTester tester = new ApacheHTTPComponentsTester("localhost", 8080, "/api/persons");

    	String data = tester.getData();
    	
    	ObjectMapper mapper = new ObjectMapper();
    	//PersonListResponse responseObj = mapper.readValue(data, PersonListResponse.class);

		ApiResponse<Person[]> responseObj = mapper.readValue(data, new TypeReference<ApiResponse<Person[]>>() {});

    	for (Person p : responseObj.data) {
    		System.out.println(p.toString());
    	}
    	
    	
    	
    	
    	
    	
    	JavaNetURLTester tester2 = new JavaNetURLTester("http://192.168.0.69:7083/MySpring/api/users");
    	
    	data = tester2.getData();

		ApiResponse<User[]> responseObj2 = mapper.readValue(data, new TypeReference<ApiResponse<User[]>>() {});

		for (User u : responseObj2.data) {
    		System.out.println(u.userName);
    	}

    	//Sending data
//    	Person p = new Person();
//    	p.lastName = "Baniaga";
//    	p.firstName = "Zekzek";
//    	p.middleName = "OBlea";
//    	ObjectMapper mapper = new ObjectMapper();
//    	JavaNetURLTester tester = new JavaNetURLTester("http://localhost:7083/spring-rest-test/api/persons/update");
//    	tester.postData(mapper.writeValueAsString(p));
    }
}
