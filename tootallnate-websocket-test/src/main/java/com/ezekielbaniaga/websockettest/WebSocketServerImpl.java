package com.ezekielbaniaga.websockettest;

import java.net.InetSocketAddress;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

public class WebSocketServerImpl extends WebSocketServer {

	public WebSocketServerImpl(InetSocketAddress address) {
		super(address);
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		System.out.println("Opened...");
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		System.out.println("Closed...");
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		System.out.println(message);
		sendSampleImageToAll();
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		ex.printStackTrace();
	}
	
	public void sendSampleDataToAll() {
		for (WebSocket ws : connections()) {
			ws.send("Testing from server");
		}
	}
	
	public void sendSampleImageToAll() {
		for (WebSocket ws : connections()) {
			ws.send(new TestImage().getTestImageBase64());
		}
	}

}
