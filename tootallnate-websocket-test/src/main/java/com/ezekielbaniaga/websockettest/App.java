package com.ezekielbaniaga.websockettest;

import java.net.InetSocketAddress;


/**
 * Hello world!
 * 
 */
public class App {

	public static void main(String[] args) {
		InetSocketAddress address = new InetSocketAddress("localhost", 18871);
		WebSocketServerImpl impl = new WebSocketServerImpl(address);
		impl.start();
		
//		while (true) {
//			try {
//				Thread.sleep(5000);
//				impl.sendSampleDataToAll();
//			} catch (Exception ex){}
//		}
	}
}
