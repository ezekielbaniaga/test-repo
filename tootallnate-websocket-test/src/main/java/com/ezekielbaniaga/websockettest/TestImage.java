package com.ezekielbaniaga.websockettest;

import java.io.File;
import java.nio.file.Files;

import org.java_websocket.util.Base64;

public class TestImage {

	public String getTestImageBase64() {
		String file = "C:\\Users\\Ezekiel\\Pictures\\Untitled.png";
		String base64Data = null;
		
		try {
			byte[] data = Files.readAllBytes(new File(file).toPath());
			base64Data = Base64.encodeBytes(data);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return base64Data;
	}
}
