package com.ezekielbaniaga.xtend.xtend_test;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        Person p = new Person();
        p.setAge( 29 );
        p.setFirstName( "Ezekiel" );
        p.setLastName( "Baniaga" );
        p.sayHi();
        
        Student s = new Student();
        s.setAge( 12 );
        s.setFirstName( "Johnny" );
        s.enroll();
        
        Teacher t = new Teacher( "Math", "Araling Panlipunan", "Filipino" );
        t.retrieveSubjectsInitials().forEach( System.out::println );
        
    }
}
