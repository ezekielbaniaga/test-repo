package com.ezekielbaniaga.xtend.xtend_test

import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Person {

	String lastName	

	String firstName 

	int age	
	
	def void sayHi() {
		
        val str = "2" + 7  // 27
        System.out.println(str) // dont use xtend io utility
		
		println("Hi " 
			+ lastName 
			+ ", "
			+ firstName
		)
	}
	
	def protected imProtected() {
		println("protected method")
		
		for (var i=0; i<10; i++) {
			println(i)
		}
	}
	
	override String toString() {
		"ezekiel".toFirstUpper()
	}
}

class Student extends Person {
	
	static val DEF_ID = 2
	
	String id
	
	new(String id) {
		this.id = id
	}
	
	new() {
		this(String.valueOf(DEF_ID))
    }
    
    def void enroll() {
    	println("I'm enrolled")

    	imProtected();
    }
	
}

class Teacher extends Person {

	String[] subjects;
	
	new (String... subjects){
		this.subjects = subjects
	}
	
	def retrieveSubjectsInitials() { // inferred function
		subjects.map[ s | s.substring(0, 1)]
	}
	
}