package com.ezekielbaniaga.xtend.xtend_test

class MyUtility {
	
	def static convertToSubstandardJSON(String s) {
		"{" + s + "}"
	}
	
	def static testLiteral() {
		val b = true;
		
		val f = if (b) {
			println('b is true') 
			"hi" 
		} else { 
			println('b is false')
			"ho" 
		}
		
		println(f)
	}
	
	def static testCast() {
		val f = 1.0f
		
		println(f as double)
		
	}
	
	def static void main(String... args) {
		val subStandard = "hi".convertToSubstandardJSON // calling extension method
		println(subStandard)
		
		MyUtility.testLiteral
	}
}