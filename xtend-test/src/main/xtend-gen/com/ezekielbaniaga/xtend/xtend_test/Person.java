package com.ezekielbaniaga.xtend.xtend_test;

import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.Pure;

@Accessors
@SuppressWarnings("all")
public class Person {
  private String lastName;
  
  private String firstName;
  
  private int age;
  
  public void sayHi() {
    final String str = ("2" + Integer.valueOf(7));
    System.out.println(str);
    InputOutput.<String>println(
      ((("Hi " + this.lastName) + ", ") + this.firstName));
  }
  
  public String toString() {
    return "ezekiel";
  }
  
  @Pure
  public String getLastName() {
    return this.lastName;
  }
  
  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }
  
  @Pure
  public String getFirstName() {
    return this.firstName;
  }
  
  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }
  
  @Pure
  public int getAge() {
    return this.age;
  }
  
  public void setAge(final int age) {
    this.age = age;
  }
}
