package com.ezekielbaniaga.sparkjob.sparkjobtest;

import static com.datastax.spark.connector.CassandraJavaUtil.javaFunctions;

import java.io.Serializable;
import java.util.Iterator;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;

import com.datastax.spark.connector.rdd.CassandraJavaRDD;
import com.fasterxml.jackson.databind.ObjectMapper;


public class CassandraIndexer {

	@SuppressWarnings("serial")
	public static void main(String[] args) {
    	final ObjectMapper mapper = new ObjectMapper();
    	
        SparkConf conf = new SparkConf()
        	.setAppName("ES-Cass Integrator")
        	.setMaster("spark://192.168.0.147:7077")
        	.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
			.set("spark.kryo.registrator", "com.ezekielbaniaga.sparkjob.sparkjobtest.Registrator")
			.set("spark.cassandra.connection.host", "192.168.0.141");

        JavaSparkContext sc = new JavaSparkContext(conf);
        
        // read Cassandra table
    	CassandraJavaRDD<Person> cassRDD = javaFunctions(sc).cassandraTable("personsspace", "persons", Person.class);

    	// Let's cache it. Save some CPU for transformations
    	cassRDD.cache();
    	
        // each JSON object, send to ElasticSearch
        cassRDD.foreachPartition(new VoidFunction<Iterator<Person>>() {
        	
        	public void call(Iterator<Person> persons) throws Exception {
		    	ElasticSearchDriverFeeder feeder = new ElasticSearchDriverFeeder("192.168.0.166", "9300");
        		
		    	while(persons.hasNext()) {
		    		Person p = persons.next();

					String json = mapper.writeValueAsString(p);
					feeder.feedData("persons", "student", String.valueOf(p.getId()), json);
		    	}
        		
		        feeder.flushRemaining();
		        feeder.close();
        	};
        });
        
	}
	
	
	public static class Person implements Serializable {
		private String prefix;
		private int id;
		private int age;
		private String name;
		
		// this is important
		public Person() { }
		
		public String getPrefix() {
			return prefix;
		}
		public void setPrefix(String prefix) {
			this.prefix = prefix;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	

}
