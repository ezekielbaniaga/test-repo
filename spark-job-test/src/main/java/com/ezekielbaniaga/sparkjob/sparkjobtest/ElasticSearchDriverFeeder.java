package com.ezekielbaniaga.sparkjob.sparkjobtest;

import java.io.Serializable;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

public class ElasticSearchDriverFeeder implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final int BULK_SIZE = 100000; // Hundred thousand per batch

	private Client client;
	private BulkRequestBuilder bulk;
	
	private String host;
	private String port;
	
	public ElasticSearchDriverFeeder(String host, String port) {
		this.host = host;
		this.port = port;
	}
	
	public void close() {
		getClient().close();
	}
	
	@SuppressWarnings("resource")
	private Client getClient() {
		if (client == null) {
			client = new TransportClient()
				.addTransportAddress(new InetSocketTransportAddress(host, Integer.parseInt(port)));
		}

		return client;
	}
	
	private BulkRequestBuilder getBulk() {
		if (bulk == null) {
			bulk = initBulk();
		}
		
		return bulk;
	}

	private BulkRequestBuilder initBulk() {
		BulkRequestBuilder bulk = getClient().prepareBulk();
		bulk.setRefresh(false);
		return bulk;
	}

	private void commitBulk() {
		BulkResponse bulkResponse = getBulk().execute().actionGet();
		if (bulkResponse.hasFailures()) {
			System.out.println("Has errors during indexing:");
			System.out.println(bulkResponse.buildFailureMessage());
		} else {
			System.out.println("Bulk done in " + bulkResponse.getTookInMillis() + "ms");
		}
	}
	
	public void feedData(String index, String type, String id, String json) {
		getBulk().add(
			new IndexRequest(index, type, id).source(json.getBytes())
		);

		if (getBulk().numberOfActions() == BULK_SIZE) {
			commitBulk();
			bulk = initBulk();
		}
	}
	
	public void flushRemaining() {
		if (getBulk().numberOfActions() > 0) {
			commitBulk();
		}
	}
}
