package com.ezekielbaniaga.sparkjob.sparkjobtest;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

/**
 * Sample standalone Spark job using Java flavour
 */
public class App {

    @SuppressWarnings("serial")
	public static void main( String[] args ) {
        String logFile = "/usr/local/spark-1.0.2-bin-hadoop2/README.md"; 
        SparkConf conf = new SparkConf()
        	.setMaster("spark://192.168.0.147:7077")
        	.setAppName("Zek Test Application");
        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaRDD<String> logData = sc.textFile(logFile).cache();

        long numAs = logData.filter(new Function<String, Boolean>() {
          public Boolean call(String s) { return s.contains("a"); }
        }).count();

        long numBs = logData.filter(new Function<String, Boolean>() {
          public Boolean call(String s) { return s.contains("b"); }
        }).count();

        System.out.println("Lines with a: " + numAs + ", lines with b: " + numBs);
    }
}
