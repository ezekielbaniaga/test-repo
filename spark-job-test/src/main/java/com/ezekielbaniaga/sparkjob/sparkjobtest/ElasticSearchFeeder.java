package com.ezekielbaniaga.sparkjob.sparkjobtest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.DefaultBHttpClientConnection;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpProcessorBuilder;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestExpectContinue;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.RequestUserAgent;

public class ElasticSearchFeeder implements Serializable {

	private static final long serialVersionUID = 1L;

	private HttpHost host;
	private HttpRequestExecutor httpexecutor;
	private HttpProcessor httpproc;
	private DefaultBHttpClientConnection conn;
	private ConnectionReuseStrategy connStrategy;
	private HttpCoreContext coreContext;

	public ElasticSearchFeeder(String _host, int _port) {
		super();

		httpproc = HttpProcessorBuilder.create()
				.add(new RequestContent()).add(new RequestTargetHost())
				.add(new RequestConnControl())
				.add(new RequestUserAgent("Spark/1.1"))
				.add(new RequestExpectContinue(true)).build();

		httpexecutor = new HttpRequestExecutor();

		HttpCoreContext coreContext = HttpCoreContext.create();
		host = new HttpHost(_host, _port);
		coreContext.setTargetHost(host);

		conn = new DefaultBHttpClientConnection(
				8 * 1024);
		connStrategy = DefaultConnectionReuseStrategy.INSTANCE;
	}

	public void feedData(String index, String type, String id, String json) {
		try {

            HttpEntity requestBody = new InputStreamEntity(
                new ByteArrayInputStream(json.getBytes("UTF-8")),
                ContentType.APPLICATION_JSON);

            if (!conn.isOpen()) {
                Socket socket = new Socket(host.getHostName(), host.getPort());
                conn.bind(socket);
            }

            BasicHttpEntityEnclosingRequest request = new BasicHttpEntityEnclosingRequest("POST", 
            	String.format("/%s/%s/%d/", index, type, id));
            request.setEntity(requestBody);

            httpexecutor.preProcess(request, httpproc, coreContext);
            HttpResponse response = httpexecutor.execute(request, conn, coreContext);
            httpexecutor.postProcess(response, httpproc, coreContext);

            if (!connStrategy.keepAlive(response, coreContext)) {
                conn.close();
            } else {
                //System.out.println("Connection kept alive...");
            }
		} catch (Exception ex) {
			ex.printStackTrace();
        } finally {
            try {
				conn.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
	}
}
