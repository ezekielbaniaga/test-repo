package com.ezekielbaniaga.sparkjob.sparkjobtest;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

public class ElasticSearchBasicFeeder implements Serializable {

	private static final long serialVersionUID = 1L;

	private String host;
	private String port;
	
	public ElasticSearchBasicFeeder (String _host, int _port) {
		this.host = _host;
		this.port = String.valueOf(_port);
	}

	public void feedData(String index, String type, String id, String json) {
		try {
			URL url = new URL("http://" + host + ":" + port + "/" + index + "/" + type
					+ "/" + id);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setUseCaches(false);
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");
			conn.connect();

			conn.getOutputStream().write(json.getBytes("UTF-8"));
//			conn.getOutputStream().close();
			
			System.out.println(conn.getResponseCode());
			System.out.println(conn.getResponseMessage());

			conn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
