package com.ezekielbaniaga.sparkjob.sparkjobtest;

import org.apache.spark.serializer.KryoRegistrator;

import com.esotericsoftware.kryo.Kryo;

public class Registrator implements KryoRegistrator {

	@Override
	public void registerClasses(Kryo k) {
		k.register(ElasticSearchDriverFeeder.class);
	}

}
