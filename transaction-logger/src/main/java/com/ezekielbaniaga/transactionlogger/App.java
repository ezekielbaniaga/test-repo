package com.ezekielbaniaga.transactionlogger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Plain JDBC transaction logger.
 */
public class App { 
	
	public static void main(String[] args) throws Exception {
		String dbName = "cs3experimentdb";

	    String userName = "postgres";
        String password = "m@s@nt1ng";
        String url = "jdbc:postgresql://192.168.0.204:5432/" + dbName;

		Connection conn = DriverManager.getConnection(url, userName, password);
		PreparedStatement ps = conn.prepareStatement("select * from pg_stat_database where datname like ?");
		ps.setString(1, dbName);

		long lastTransaction = 0;
		long newTransaction = 0;
		long tcps = 0;

		while (true) {
			ResultSet rs = ps.executeQuery();
			rs.next();
			
			System.out.println("Transaction Count:" + tcps);
			
			newTransaction = (rs.getLong("xact_commit") + rs.getLong("xact_rollback"));
			tcps = newTransaction - lastTransaction;
			lastTransaction = newTransaction;
			
			Thread.sleep(1000);
		}
    }
}
