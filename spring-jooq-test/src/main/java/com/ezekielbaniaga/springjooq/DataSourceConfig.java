package com.ezekielbaniaga.springjooq;

import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.jolbox.bonecp.BoneCPDataSource;

/**
 * This configuration will instantiate the default DSLContext.
 * 
 *     PostgreSQL + Spring Transaction + BoneCP config
 * 
 * @author Ezekiel
 */
@Configuration
@EnableTransactionManagement
public class DataSourceConfig {

	/**
	 * Configure jOOQ datasource
	 * @return
	 */
	@Bean(destroyMethod="close")
	public BoneCPDataSource dataSource() {
		BoneCPDataSource dataSource = new BoneCPDataSource();
		dataSource.setDriverClass("org.postgresql.Driver");
		dataSource.setJdbcUrl("jdbc:postgresql://192.168.0.204:5432/zekdb");
		dataSource.setUsername("postgres");
		dataSource.setPassword("m@s@nt1ng");
		
		return dataSource;
	}
	
	/**
	 * Configure Spring's transaction manager to use dataSource().
	 * @return
	 */
	@Bean
	public DataSourceTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}
	
	/**
	 * Configure jOOQ's ConnectionProvider to use Spring's TransactionAwareDataSourceProxy,
	 * which can dynamically discover the transaction context.
	 * @return
	 */
	@Bean
	public TransactionAwareDataSourceProxy transactionAwareDataSource() {
		return new TransactionAwareDataSourceProxy(dataSource());
	}
	
	@Bean
	public DataSourceConnectionProvider connectionProvider() {
		return new DataSourceConnectionProvider(transactionAwareDataSource());
	}
	
	@Bean
	public ExceptionTranslator exceptionTranslator() {
		return new ExceptionTranslator();
	}

	@Bean
	public DefaultConfiguration config() {
		DefaultConfiguration config = new DefaultConfiguration();

		config.set(connectionProvider())
			  .set(new DefaultExecuteListenerProvider(exceptionTranslator()))
			  .set(SQLDialect.POSTGRES);

		return config;
	}
	
	/**
	 * Configure the DSL object, optionally overriding jOOQ Exceptions with Spring Exceptions
	 */
	@Bean
	public DefaultDSLContext dsl() {
		return new DefaultDSLContext(config());
	}
}
