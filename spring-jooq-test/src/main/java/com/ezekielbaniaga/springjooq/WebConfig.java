package com.ezekielbaniaga.springjooq;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Declare Web MVC configurations here such as view resolvers.
 * 
 * @author Ezekiel
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages={
	"com.ezekielbaniaga.springjooq.controllers"
})
public class WebConfig {

}
