package com.ezekielbaniaga.springjooq.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ezekielbaniaga.springjooq.models.ApiResponse;
import com.ezekielbaniaga.springresttest.services.AuthorsService;

@Controller
@RequestMapping("/authors")
public class AuthorsController {

	@Autowired
	AuthorsService authorsService;
	
	@ResponseBody
	@RequestMapping(method=RequestMethod.GET)
	public ApiResponse list() {
		return ApiResponse.createSuccessResponse(authorsService.getAllAuthors());
	}
}
