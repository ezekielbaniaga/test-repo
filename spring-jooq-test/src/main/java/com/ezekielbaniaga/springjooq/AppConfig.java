package com.ezekielbaniaga.springjooq;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ezekielbaniaga.springresttest.dao.AuthorsDAO;
import com.ezekielbaniaga.springresttest.dao.AuthorsDAOImpl;
import com.ezekielbaniaga.springresttest.services.AuthorsService;
import com.ezekielbaniaga.springresttest.services.AuthorsServiceImpl;

/**
 * Declare Application Context here. e.g. beans and controllers
 * 
 * @author Ezekiel
 */
@Configuration
@ComponentScan(basePackages={
	"com.ezekielbaniaga.springjooq.dao",
	"com.ezekielbaniaga.springjooq.services",
})
public class AppConfig {

	/* DAO Layers */
	
	/**
	 * Authors DAO default impl
	 */
	@Bean
	AuthorsDAO authorsDao() {
		return new AuthorsDAOImpl();
	}

	/* Service Layers */
	
	/**
	 * Authors Service default impl
	 */
	@Bean
	AuthorsService authorsService() {
		return new AuthorsServiceImpl();
	}
}
