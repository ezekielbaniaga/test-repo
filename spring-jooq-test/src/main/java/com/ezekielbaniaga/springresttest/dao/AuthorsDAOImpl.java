package com.ezekielbaniaga.springresttest.dao;

import static com.ezekielbaniaga.springjooq.jooq.tables.Authors.AUTHORS;

import java.util.ArrayList;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ezekielbaniaga.springjooq.models.Author;

@Repository
public class AuthorsDAOImpl implements AuthorsDAO {

	@Autowired
	DSLContext ctx;
	
	public List<Author> findAllAuthors() {
		Result<Record> result = ctx.select().from(AUTHORS).fetch();
		
		List<Author> authors = new ArrayList<Author>();
		for (Record record : result) {
			Author a = new Author();

			a.authorId = record.getValue(AUTHORS.AUTHORID);
			a.name     = record.getValue(AUTHORS.AUTHORNAME);
			a.age 	   = record.getValue(AUTHORS.AGE);
			a.info 	   = record.getValue(AUTHORS.INFO);
			
			authors.add(a);
		}
		
		return authors;
	}

}
