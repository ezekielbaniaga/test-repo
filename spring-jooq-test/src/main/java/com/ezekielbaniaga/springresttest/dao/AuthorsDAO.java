package com.ezekielbaniaga.springresttest.dao;

import java.util.List;

import com.ezekielbaniaga.springjooq.models.Author;

public interface AuthorsDAO {

	public List<Author> findAllAuthors();
}
