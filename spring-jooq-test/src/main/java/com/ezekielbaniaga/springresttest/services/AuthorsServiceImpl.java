package com.ezekielbaniaga.springresttest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ezekielbaniaga.springjooq.models.Author;
import com.ezekielbaniaga.springresttest.dao.AuthorsDAO;

@Service
public class AuthorsServiceImpl implements AuthorsService {
	
	@Autowired
	AuthorsDAO authorsDao;

	public List<Author> getAllAuthors() {
		List<Author> authors = authorsDao.findAllAuthors();
		
		for (Author a : authors) {
			a.name = a.name.toUpperCase();
		}

		return authors;
	}

}
