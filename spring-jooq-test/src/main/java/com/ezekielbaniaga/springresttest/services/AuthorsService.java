package com.ezekielbaniaga.springresttest.services;

import java.util.List;

import com.ezekielbaniaga.springjooq.models.Author;

public interface AuthorsService {
	List<Author> getAllAuthors();
}
